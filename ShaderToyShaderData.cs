﻿public class Shadertoy
{
	public string ver { get; set; }
	public Info info { get; set; }
	public Renderpass[] renderpass { get; set; }
}

public class Info
{
	public string id { get; set; }
	public string date { get; set; }
	public int viewed { get; set; }
	public string name { get; set; }
	public string description { get; set; }
	public int likes { get; set; }
	public string published { get; set; }
	public int usePreview { get; set; }
	public string[] tags { get; set; }
	public int hasliked { get; set; }
	public string parentid { get; set; }
	public string parentname { get; set; }
}

public class Renderpass
{
	public object[] inputs { get; set; }
	public Output[] outputs { get; set; }
	public string code { get; set; }
	public string name { get; set; }
	public string description { get; set; }
	public string type { get; set; }
}

public class Output
{
	public string id { get; set; }
	public int channel { get; set; }
}
