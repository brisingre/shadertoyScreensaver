﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShadertoyScreensaver
{
	public static class GlslSnippets
	{
		public const string UNIFORM_SAMPLER_2D = "uniform sampler2D";
		public const string UNIFORM_SAMPLER_3D = "uniform sampler3D";
		public const string UNIFORM_SAMPLER_CUBE = "uniform samplerCube";

		public const string INCLUDE_SHADERTOY_COMMON = "//INCLUDE_SHADERTOY_COMMON";

		public const string INCLUDE_SHADERTOY_RENDERPASS = "//INCLUDE_SHADERTOY_RENDERPASS";

		public const string INCLUDE_SHADERTOY_SAMPLER_0 = "//INCLUDE_SHADERTOY_SAMPLER_0";
		public const string INCLUDE_SHADERTOY_SAMPLER_1 = "//INCLUDE_SHADERTOY_SAMPLER_1";
		public const string INCLUDE_SHADERTOY_SAMPLER_2 = "//INCLUDE_SHADERTOY_SAMPLER_2";
		public const string INCLUDE_SHADERTOY_SAMPLER_3 = "//INCLUDE_SHADERTOY_SAMPLER_3";

	}

	public static class PathStrings
	{
		public const string ASSETS_FOLDER = "Assets";
		public const string COMPANY_NAME = "Seagull Incident";
		public const string APP_NAME = "Shadertoy Screensaver";
		public const string SAVEFILE_NAME = "config.json";
	}

	public static class RegistryKeys
	{

		public const string SHADER_NAME = "shaderName";
		public const string FRAME_RATE = "frameRate";
		public const string SHADERTOY_URL = "shadertoyId";
		public const string FULLSCREEN_MODE = "fullscreenMode";

	}

}
