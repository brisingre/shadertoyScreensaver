﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Drawing.Drawing2D;
using System.IO;
using System.Text;

namespace ShadertoyScreensaver
{

	public enum WrapMode
	{
		CLAMP = (int)OpenGL.GL_CLAMP_TO_EDGE,//IMPROVED GUESS
		REPEAT = (int)OpenGL.GL_REPEAT
	}

	public enum FilterMode
	{
		MIPMAP = (int)OpenGL.GL_LINEAR_MIPMAP_LINEAR,//GUESS
		LINEAR = (int)OpenGL.GL_LINEAR,
		NEAREST = (int)OpenGL.GL_NEAREST,
	}
	public enum PixelFormat
	{
		RED = (int)OpenGL.GL_RED,
		RGB = (int)OpenGL.GL_RGB,
		RGBA = (int)OpenGL.GL_RGBA,
	}

	public static class OpenGlEnums
	{
		public static WrapMode WrapModeFromString(string name)
		{
			switch (name.ToLower())
			{
				case "clamp":
					return WrapMode.CLAMP;
				case "repeat":
					return WrapMode.REPEAT;
				default:
					throw new Exception("Bad string in wrapMode");
			}
		}

		public static FilterMode FilterModeFromString(string name)
		{

			switch (name.ToLower())
			{
				case "mipmap":
					return FilterMode.MIPMAP;
				case "linear":
					return FilterMode.LINEAR;
				case "nearest":
					return FilterMode.NEAREST;
				default:
					throw new Exception("Bad string in filterMode");
			}
		}
	}

}
