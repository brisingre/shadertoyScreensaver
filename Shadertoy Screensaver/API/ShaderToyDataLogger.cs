﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ShadertoyScreensaver.API
{
	public static class ShaderToyDataLogger
	{

		public static string FromShaderToyDataRenderPass(ShaderToyDataRenderPass data, bool code = true, bool deeper = true)
		{
			var retval = $"ShaderToyDataInput\n" +
				$"	Name: {data.Name}\n" +
				$"	Type: {data.Type}\n" +
				$"	Description: {data.Description}\n";
			if(code)
			{
				retval += $"	Code:\n";
				retval += PadLines(data.Code, "\t\t");
			}

			if (deeper)
			{
				retval += $"	Inputs:\n";
				foreach (var input in data.Inputs)
				{
					var inputString = FromShaderToyDataInput(input);
					retval += PadLines(inputString);
				}
				retval += $"	Outputs:\n";
				foreach (var output in data.Outputs)
				{
					var outputString = FromShaderToyDataOutput(output);
					retval += PadLines(outputString);
				}
			}
			return retval;
		}


		public static string FromShaderToyDataInput(ShaderToyDataInput data, bool deeper = true)
		{
			var retval = $"ShaderToyDataInput\n" +
				$"	ID: {data.Id}\n" +
				$"	Src: {data.Src}\n" +
				$"	Ctype: {data.Ctype}\n" +
				$"	Channel: {data.Channel}\n" +
				$"	Published: {data.Published}\n";
			if (deeper)
			{
				var samplerstring = FromShaderToyDataSampler(data.Sampler);
				retval += PadLines(samplerstring);
			}
			return retval;
		}

		public static string FromShaderToyDataOutput(ShaderToyDataOutput data)
		{
			var retval = $"ShaderToyDataOutput\n" +
				$"	Id: {data.Id}\n" +
				$"	Channel: {data.Channel}\n";
			return retval;
		}

		public static string FromShaderToyDataSampler(ShaderToyDataSampler data)
		{
			var retval = $"ShaderToyDataSampler\n" +
				$"	Filter: {data.Filter}\n" +
				$"	Wrap: {data.Wrap}\n" +
				$"	Vflip: {data.Vflip}\n" +
				$"	Srgb: {data.Srgb}\n" +
				$"	Internal: {data.Internal}\n";
			return retval;
		}


		public static string PadLines(string input, string padding = "	")
		{
			var lines = input.Split("\n");
			var retval = "";
			foreach( var line in lines)
			{
				retval += $"{padding}{line}\n";
			}
			return retval;
		}
	}
}
