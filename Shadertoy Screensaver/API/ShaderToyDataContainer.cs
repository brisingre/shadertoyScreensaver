﻿using System.Collections.Generic;

namespace ShadertoyScreensaver.API
{
	public class ShaderToyDataContainer
	{
		public ShaderToyDataShader Shader { get; set; }
	}

	public class ShaderToyDataShader
	{
		public string Ver { get; set; }
		public ShaderToyDataInfo Info { get; set; }
		public List<ShaderToyDataRenderPass> RenderPass { get; set; }
	}

	public class ShaderToyDataInfo
	{
		public string Id { get; set; }
		public string Date { get; set; }
		public int Viewed { get; set; }
		public string Name { get; set; }
		public string Username { get; set; }
		public string Description { get; set; }
		public int Likes { get; set; }
		public int Published { get; set; }
		public int Flags { get; set; }
		public int UsePreview { get; set; }
		public List<string> Tags { get; set; }
		public int HasLiked { get; set; }
	}

	public class ShaderToyDataRenderPass
	{
		public List<ShaderToyDataInput> Inputs { get; set; }
		public List<ShaderToyDataOutput> Outputs { get; set; }
		public string Code { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public string Type { get; set; }
	}

	public class ShaderToyDataInput
	{
		public int Id { get; set; }
		public string Src { get; set; }
		public string Ctype { get; set; }
		public int Channel { get; set; }
		public ShaderToyDataSampler Sampler { get; set; }
		public int Published { get; set; }
	}

	public class ShaderToyDataSampler
	{
		public string Filter { get; set; }
		public string Wrap { get; set; }
		public string Vflip { get; set; }
		public string Srgb { get; set; }
		public string Internal { get; set; }
	}

	public class ShaderToyDataOutput
	{
		public int Id { get; set; }
		public int Channel { get; set; }
	}
}