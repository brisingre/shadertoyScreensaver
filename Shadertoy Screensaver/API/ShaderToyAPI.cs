﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing.Printing;
using System.IO;
using System.Net.Http;
using System.Security.Cryptography;
using System.Security.Policy;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace ShadertoyScreensaver.API
{
	public class ShaderToyAPI
	{
		private readonly HttpClient client;
		private readonly string apiKey;

		public ShaderToyAPI(string apiKey)
		{
			this.apiKey = apiKey;
			client = new HttpClient();
		}

		public async Task<string> FetchStringApiAsync(string url)
		{
			Debug.WriteLine($"Fetching {url}");
			try
			{
				HttpResponseMessage response = await client.GetAsync(url);
				response.EnsureSuccessStatusCode();
				string responseBody = await response.Content.ReadAsStringAsync();
				return responseBody;
			}
			catch (HttpRequestException e)
			{
				Debug.WriteLine("\nException Caught!");
				Debug.WriteLine("Message :{0} ", e.Message);
				return null;  // Or handle the exception as needed
			}
		}



		public async Task<byte[]> FetchFileApiAsync(string url)
		{
			Debug.WriteLine($"Fetching {url}");
			try
			{
				HttpResponseMessage response = await client.GetAsync(url, HttpCompletionOption.ResponseHeadersRead);
				response.EnsureSuccessStatusCode();

				using (var memoryStream = new MemoryStream())
				{
					await response.Content.CopyToAsync(memoryStream);
					return memoryStream.ToArray();  // Returns the downloaded file as a byte array
				}
			}
			catch (HttpRequestException e)
			{
				Debug.WriteLine("\nException Caught!");
				Debug.WriteLine("Message :{0} ", e.Message);
				return null;  // Or handle the exception as needed
			}
		}



		// Fetch IDs for a number of shaders. 
		public async Task<string> FetchShaderIDsAsync(int numShaders = 1)
		{
			string url = $"https://www.shadertoy.com/api/v1/shaders?key={this.apiKey}&num={numShaders}";

			return await FetchStringApiAsync(url);

		}


		//Fetch a whole shader from its ID
		public async Task<ShaderToyDataContainer> FetchShaderFromIDAsync(string shaderID)
		{
			string url = $" https://www.shadertoy.com/api/v1/shaders/{shaderID}?key={this.apiKey}";

			string json = await FetchStringApiAsync(url);

			return JsonConvert.DeserializeObject<ShaderToyDataContainer>(json);

		}

		//Fetch a binary asset from given a partial URL
		public async Task<Byte[]> FetchAssetFromSrcAsync(string src)
		{
			var localFile = Helpers.LocalPathFromShadertoySrc(src);

			if (File.Exists(localFile))
			{
				var bytes = await File.ReadAllBytesAsync(localFile);
				return bytes;
			}
			else
			{
				string url = $" https://www.shadertoy.com/{src}";
				var result = await FetchFileApiAsync(url);
				if(result != null)
				{
					MessageBox.Show($"Creating new file: {localFile}. If it's a cubemap, it will require manual replacement.");

					await File.WriteAllBytesAsync(localFile, result);
				}
				return result;
			}

		}

		//Fetch all the binary assets for a shader
		public async Task<Dictionary<string, Byte[]>> FetchAssetsFromStdc(ShaderToyDataContainer stdc)
		{
			var retval = new Dictionary<string, Byte[]>();
			foreach (var renderPass in stdc.Shader.RenderPass)
			{
				Debug.WriteLine($"RenderPass: {renderPass.Name}");
				foreach (var input in renderPass.Inputs)
				{
					Debug.WriteLine($"{input.Src} {input}");
					if (retval.ContainsKey(input.Src))
					{
						Debug.WriteLine($"{input.Src} is already in the dictionary.");
					}
					else
					{
						var fetched = await FetchAssetFromSrcAsync(input.Src);
						retval.Add(input.Src, fetched);
					}
				}
			}
			return retval;
		}

		//Fetch a shader and all its assets
		public async Task<(ShaderToyDataContainer ShaderToyDataContainer, Dictionary<string, byte[]> ShaderToyAssets)> FetchShaderAndAssetsFromIDAsync(string shaderID)
		{
			var stdc = await FetchShaderFromIDAsync(shaderID);
			var assets = await FetchAssetsFromStdc(stdc);
			return (ShaderToyDataContainer: stdc, ShaderToyAssets: assets);
		}
	}
}