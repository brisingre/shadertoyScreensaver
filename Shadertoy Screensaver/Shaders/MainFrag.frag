﻿#version 400 core
in vec3 pass_Color;
in vec2 fragCoord;
out vec4 fragColor;

uniform vec3      iResolution;           // viewport resolution (in pixels)
uniform float     iTime;                 // shader playback time (in seconds)
uniform float     iTimeDelta;            // render time (in seconds)
uniform float     iFrameRate;            // shader frame rate
uniform int       iFrame;                // shader playback frame
uniform float     iChannelTime[4];       // channel playback time (in seconds)
uniform vec3      iChannelResolution[4]; // channel resolution (in pixels)
uniform vec4      iMouse;                // mouse pixel coords. xy: current (if MLB down), zw: click
uniform vec4      iDate;                 // (year, month, day, time in seconds)
//uniform float     iSampleRate;           // sound sample rate (i.e., 44100)

//INCLUDE_SHADERTOY_SAMPLER_0 iChannel0;          // input channel. XX = 2D/Cube
//INCLUDE_SHADERTOY_SAMPLER_1 iChannel1;          // input channel. XX = 2D/Cube
//INCLUDE_SHADERTOY_SAMPLER_2 iChannel2;          // input channel. XX = 2D/Cube
//INCLUDE_SHADERTOY_SAMPLER_3 iChannel3;          // input channel. XX = 2D/Cube

//INCLUDE_SHADERTOY_COMMON

//INCLUDE_SHADERTOY_RENDERPASS


void main()
{
    mainImage(fragColor, fragCoord);
}

