using LibVLCSharp.Shared;
using Microsoft.Win32;
using ShadertoyScreensaver.Config;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ShadertoyScreensaver
{
	static class Program
	{
		public const string REGISTRY_KEY = "Seagull Incident\\ShadertoyScreensaver";
		public const string API_KEY = "rdnK41";

		static List<ScreenSaverForm> screensavers = new List<ScreenSaverForm>();

		[STAThread]
		static void Main(string[] args)
		{
			Application.SetHighDpiMode(HighDpiMode.SystemAware);
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);

			Startup();

			//args = new string[1] { "/s" };
			if(args.Length > 0)
			{
				string firstArgument = args[0].ToLower().Trim();
				string secondArgument = null;

				if (firstArgument.Length > 2)
				{
					secondArgument = firstArgument.Substring(3).Trim();
					firstArgument = firstArgument.Substring(0, 2);
				}
				else if (args.Length > 1)
				{ 
					secondArgument = args[1]; 
				}

				switch(firstArgument)
				{
					case "/c":
						//Configuration mode
						Application.Run(new SettingsForm());
						break;
					case "/p":
						//Preview mode
						if(secondArgument == null)
						{
							MessageBox.Show("Preview broke because the expected window handle was not provided.");
							return;
						}
						else
						{
							IntPtr previewWndHandle = new IntPtr(long.Parse(secondArgument));
							Application.Run(new ScreenSaverForm(previewWndHandle));
						}
						break;
					case "/s":
						//Full-Screen mode
						ShowScreenSaver();
						Application.Run();

						break;
					default:
						MessageBox.Show("Not sure what to do with command line arg: " + firstArgument);
						break;
				}
			}
			else
			{
				//No arguments -- treat like /c 
				Application.Run(new SettingsForm());

				//Application.Run(new APIForm());
			}
		}
	
		public static void ShowScreenSaver()
		{
			var config = Helpers.LoadConfig();

			if(config.Fullscreen)
			{
				foreach (Screen screen in Screen.AllScreens)
				{
					var screensaver = new ScreenSaverForm(screen.Bounds);
					screensavers.Add(screensaver);
					screensaver.FormBorderStyle = FormBorderStyle.None;
					screensaver.Show();
				}
			}
			else
			{
				var screensaver = new ScreenSaverForm(new System.Drawing.Rectangle(100, 100, 1200, 675));
				screensavers.Add(screensaver);
				screensaver.FormBorderStyle = FormBorderStyle.Fixed3D;
				screensaver.Show();
			}
		}

		public static void Startup()
		{
			Directory.CreateDirectory(PathStrings.ASSETS_FOLDER); //Make sure the Assets directory exists

			Directory.CreateDirectory(Path.GetDirectoryName(ShadertoyScreensaverConfig.SavePath));
			Core.Initialize(); //Initialize VLC
		}

		public static void KillScreenSaver()
		{
			foreach (var s in screensavers)
				s.Quit();
		}
	}
}
