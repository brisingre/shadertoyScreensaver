﻿using GlmNet;
using Microsoft.Win32;
using SharpGL;
using SharpGL.Shaders;
using SharpGL.VertexBuffers;
using ShadertoyScreensaver.API;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows.Forms;
using System.Linq;

namespace ShadertoyScreensaver.Scenes
{
	internal class ShadertoyApiScene : BaseScene
	{
		internal class ShaderGlobals
		{
			//  The projection, view and model matrices.
			public mat4 projectionMatrix;
			public mat4 viewMatrix;
			public mat4 modelMatrix;

			//  The vertex buffer array which contains the vertex and colour buffers.
			public VertexBufferArray vertexBufferArray;

			// Shadertoy Inputs
			//iResolution isn't global, it's different on Cubemaps apparently
			public float iTime;
			public float iTimeDelta;
			public float iFrameRate; 
			public int iFrame = -1;
			//iChannelTime is from Texture, not global
			//iChannelResolution is from Texture, not 
			public float[] iMouse = new float[4];
			public float[] iDate = new float[4];
			//iSampleRate isn't used yet

			//iChannel0-4 are not global
		}

		//Initialization State
		bool openGLIsReady = false;
		bool apiDataIsReady = false;




		//  The shader program for our vertex and fragment shader.
		public ShaderProgram blitShaderProgram;


		public ShaderGlobals shaderGlobals = new ShaderGlobals();

		public string commonRenderPassCode = "";

		public RenderPassBuffer renderPassBufferImage;

		public RenderPassBuffer[] renderPassBuffers = new RenderPassBuffer[4];

		public RenderPassBuffer renderPassBufferA { get { return renderPassBuffers[0]; } set{ renderPassBuffers[0] = value; } }
		public RenderPassBuffer renderPassBufferB { get { return renderPassBuffers[1]; } set { renderPassBuffers[1] = value; } }
		public RenderPassBuffer renderPassBufferC { get { return renderPassBuffers[2]; } set { renderPassBuffers[2] = value; } }
		public RenderPassBuffer renderPassBufferD { get { return renderPassBuffers[3]; } set { renderPassBuffers[3] = value; } }


		string shadertoyId = "ftyGzW";

		public ShaderToyDataContainer shaderToyDataContainer;
		public Dictionary<string, byte[]> shaderToyAssets;

		int width;
		int height;


		float[] mouseLocation = new float[2];
		bool lmbHeld = false;
		bool lmbHeldLastFrame = false;



		public ShadertoyApiScene(OpenGL gl) : base(gl)
		{

			var config = Helpers.LoadConfig();

			shadertoyId = config.ShadertoyURL;
			if (shadertoyId.Contains("/"))
			{
				var splits = shadertoyId.Split("/");
				shadertoyId = splits[splits.Length - 1].Trim();
			}

			
			CallAPI();
		}

		async void CallAPI()
		{
			var api = new ShaderToyAPI(Program.API_KEY);
			var results = await api.FetchShaderAndAssetsFromIDAsync(shadertoyId);
			shaderToyDataContainer = results.ShaderToyDataContainer;
			shaderToyAssets = results.ShaderToyAssets;
			apiDataIsReady = true;
			TryToSetupShaderProgram();
		}

		public override void OpenGLDraw()
		{
			if (!(openGLIsReady && apiDataIsReady))
				return;
			
			//time-related Shadertoy globals
			var tempiTime = (float)(DateTime.UtcNow - Process.GetCurrentProcess().StartTime.ToUniversalTime()).TotalSeconds;
			shaderGlobals.iTimeDelta = tempiTime - shaderGlobals.iTime;
			shaderGlobals.iFrameRate = 1.0f / shaderGlobals.iTimeDelta;
			shaderGlobals.iTime = tempiTime;
			shaderGlobals.iFrame++;
			shaderGlobals.iDate = new float[] { DateTime.Now.Year, DateTime.Now.Month - 1, DateTime.Now.Day, DateTime.Now.Hour * 3600 + DateTime.Now.Minute * 60 + DateTime.Now.Second + DateTime.Now.Millisecond / 1000f + DateTime.Now.Microsecond / 1000000f};
			
			
			//Mouse state is a little fucked
			if(lmbHeld)
			{
				//We're clicking or dragging
				shaderGlobals.iMouse[0] = mouseLocation[0];
				shaderGlobals.iMouse[1] = mouseLocation[1];

				shaderGlobals.iMouse[2] = MathF.Abs(shaderGlobals.iMouse[2]); //LMB is held
				shaderGlobals.iMouse[3] = MathF.Abs(shaderGlobals.iMouse[3]) * -1; //LMB is not clicked
				if (!lmbHeldLastFrame)
				{
					//We're a fresh click!
					shaderGlobals.iMouse[2] = mouseLocation[0];
					shaderGlobals.iMouse[3] = mouseLocation[1]; //LMB is clicked

				}

				lmbHeldLastFrame = true;

			}
			else
			{
				shaderGlobals.iMouse[2] = MathF.Abs(shaderGlobals.iMouse[2]) * -1; //LMB is not held
			}

			renderPassBufferA?.DrawShaderToBuffer(gl, shaderGlobals);
			renderPassBufferB?.DrawShaderToBuffer(gl, shaderGlobals);
			renderPassBufferC?.DrawShaderToBuffer(gl, shaderGlobals);
			renderPassBufferD?.DrawShaderToBuffer(gl, shaderGlobals);
			renderPassBufferImage?.DrawShaderToBuffer(gl, shaderGlobals);


			renderPassBufferImage?.DrawShaderToScreen(gl, shaderGlobals);
		}


		public override void OpenGLInitialized()
		{
			openGLIsReady = true;
			gl.ClearColor(0.4f, 0.0f, 0.9f, 0.0f);
			shaderGlobals.vertexBufferArray = GlslFactory.CreateVerticesForSquare(gl);
			SetupMVPMatrices();
			width = gl.RenderContextProvider.Width;
			height = gl.RenderContextProvider.Height;
			TryToSetupShaderProgram();
		}

		public override void InputMouseMove(MouseEventArgs e)
		{
			base.InputMouseMove(e);
			if (!e.Location.IsEmpty)
				mouseLocation = new float[] { e.Location.X, height - e.Location.Y};
		}

		public override void InputMouseDown(MouseEventArgs e)
		{
			base.InputMouseDown(e);
			Debug.WriteLine($"Mouse down: {e.Button}");


			if (!e.Location.IsEmpty)
				mouseLocation = new float[] { e.Location.X, height - e.Location.Y };

			if (e.Button  == MouseButtons.Left)
				lmbHeld = true;
		}
		public override void InputMouseUp(MouseEventArgs e)
		{
			base.InputMouseUp(e);
			Debug.WriteLine($"Mouse up: {e.Button}");

			if (!e.Location.IsEmpty)
				mouseLocation = new float[] { e.Location.X, height - e.Location.Y };

			if (e.Button == MouseButtons.Left)
			{
				lmbHeld = false;
				lmbHeldLastFrame = false;
			}
		}

		void SetupMVPMatrices()
		{
			shaderGlobals.projectionMatrix = glm.ortho(0, 1, 0, 1, 0, 1);
			shaderGlobals.viewMatrix = glm.translate(new mat4(1.0f), new vec3(0.0f, 0.0f, 0.0f));
			shaderGlobals.modelMatrix = glm.scale(new mat4(1.0f), new vec3(1f));
		}

		void TryToSetupShaderProgram()
		{
			if(openGLIsReady && apiDataIsReady)
			{
				//blitShaderProgram = GlslFactory.CreateShaderProgramFromLocalGlslFile(gl, "blit");

				foreach (var shaderToyDataRenderPass in shaderToyDataContainer.Shader.RenderPass)
				{
					Debug.WriteLine(ShaderToyDataLogger.FromShaderToyDataRenderPass(shaderToyDataRenderPass));
					switch (shaderToyDataRenderPass.Name)
					{
						case "Image":
							renderPassBufferImage = new RenderPassBuffer(gl, shaderToyDataRenderPass, width, height);
							break;
						case "Buffer A":
							renderPassBufferA = new RenderPassBuffer(gl, shaderToyDataRenderPass, width, height);
							break;
						case "Buffer B":
							renderPassBufferB = new RenderPassBuffer(gl, shaderToyDataRenderPass, width, height);
							break;
						case "Buffer C":
							renderPassBufferC = new RenderPassBuffer(gl, shaderToyDataRenderPass, width, height);
							break;
						case "Buffer D":
							renderPassBufferD = new RenderPassBuffer(gl, shaderToyDataRenderPass, width, height);
							break;
						case "Common":
							commonRenderPassCode = shaderToyDataRenderPass.Code;
							break;
						default:
							throw new Exception($"I don't know what to do a RenderPass called {shaderToyDataRenderPass.Name}");
					}
				}

				renderPassBufferA?.Setup(gl, this);
				renderPassBufferB?.Setup(gl, this);
				renderPassBufferC?.Setup(gl, this);
				renderPassBufferD?.Setup(gl, this);
				renderPassBufferImage?.Setup(gl, this);
				


			}
		}

		public override void Dispose()
		{
			base.Dispose();
			foreach(var buffer in renderPassBuffers)
			{
				buffer?.Dispose();
			}
			renderPassBufferImage?.Dispose();
		}
	}
}
