﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Security.Permissions;
using System.Text;
using System.Windows.Forms;

namespace ShadertoyScreensaver.Scenes
{
	internal abstract class BaseScene : IDisposable
    {
        protected OpenGL gl;
        public BaseScene(OpenGL gl)
        {
            this.gl = gl;
        }

        public abstract void OpenGLInitialized();

        public abstract void OpenGLDraw();

        public virtual void InputMouseMove(MouseEventArgs e) { }
        public virtual void InputKeyPress(KeyPressEventArgs e) { }
		public virtual void InputMouseUp(MouseEventArgs e) { }
		public virtual void InputMouseDown(MouseEventArgs e) { }

		public virtual void Dispose()
		{
            //This gets called when the object dies.
		}
	}
}
