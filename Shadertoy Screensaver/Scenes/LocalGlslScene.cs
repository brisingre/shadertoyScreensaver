﻿using GlmNet;
using Microsoft.Win32;
using ShadertoyScreensaver;
using SharpGL;
using SharpGL.SceneGraph.Lighting;
using SharpGL.Shaders;
using SharpGL.VertexBuffers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace ShadertoyScreensaver.Scenes
{
	internal class LocalGlslScene : BaseScene
	{
		
		//  The projection, view and model matrices.
		mat4 projectionMatrix;
		mat4 viewMatrix;
		mat4 modelMatrix;


		//  The vertex buffer array which contains the vertex and colour buffers.
		VertexBufferArray vertexBufferArray;

		//  The shader program for our vertex and fragment shader.
		public ShaderProgram shaderProgram;

		string localGlslShaderName;


		public LocalGlslScene(OpenGL gl) : base(gl)
		{
			RegistryKey key = Registry.CurrentUser.OpenSubKey("SOFTWARE\\" + Program.REGISTRY_KEY);
			Debug.WriteLine($"Setup Screensaver Form From {key}");
			if (key == null)
			{
				localGlslShaderName = "General SDF";
			}
			else
			{
				localGlslShaderName = (string)key.GetValue("shaderName");
			}
		}

		public override void OpenGLDraw()
		{
			gl.Clear(OpenGL.GL_COLOR_BUFFER_BIT | OpenGL.GL_DEPTH_BUFFER_BIT | OpenGL.GL_STENCIL_BUFFER_BIT);

			if (shaderProgram == null)
				return;
			
			shaderProgram.Bind(gl);

			//Set MVP Matrices
			shaderProgram.SetUniformMatrix4(gl, "projectionMatrix", projectionMatrix.to_array());
			shaderProgram.SetUniformMatrix4(gl, "viewMatrix", viewMatrix.to_array());
			shaderProgram.SetUniformMatrix4(gl, "modelMatrix", modelMatrix.to_array());

			//set iTime for Shadertoy
			var iTime = DateTime.UtcNow - Process.GetCurrentProcess().StartTime.ToUniversalTime();
			shaderProgram.SetUniform1(gl, "iTime", (float)iTime.TotalSeconds);

			//set iResolution for Shadertoy
			shaderProgram.SetUniform3(gl, "iResolution", gl.RenderContextProvider.Width, gl.RenderContextProvider.Height, 0f);


			vertexBufferArray.Bind(gl);
			gl.DrawArrays(OpenGL.GL_TRIANGLES, 0, 6);
			vertexBufferArray.Unbind(gl);

			shaderProgram.Unbind(gl);
		}

		public override void OpenGLInitialized()
		{
			gl.ClearColor(0.9f, 0.2f, 0.1f, 0.0f);
			vertexBufferArray = GlslFactory.CreateVerticesForSquare(gl);
			SetupMVPMatrices();
			shaderProgram = GlslFactory.CreateShaderProgramFromLocalGlslFile(gl, localGlslShaderName);

		}
		void SetupMVPMatrices()
		{
			projectionMatrix = glm.ortho(0, 1, 0, 1, 0, 1);
			viewMatrix = glm.translate(new mat4(1.0f), new vec3(0.0f, 0.0f, 0.0f));
			modelMatrix = glm.scale(new mat4(1.0f), new vec3(1f));
		}
	}
}
