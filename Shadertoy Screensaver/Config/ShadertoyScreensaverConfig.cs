﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShadertoyScreensaver.Config
{
	public class ShadertoyScreensaverConfig : BaseConfig
	{
		public int FrameRate = 60;
		public string ShadertoyURL = "ftyGzW";
		public bool Fullscreen = false;
		public bool KeyboardInput = false;
		public bool MouseInput = false;
		public float MouseQuitSensitivity = 5f;

	}
}
