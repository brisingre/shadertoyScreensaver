﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace ShadertoyScreensaver.Config
{
	public abstract class BaseConfig
	{
		public static string SavePath => Path.Combine(
			Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
			PathStrings.COMPANY_NAME,
			PathStrings.APP_NAME,
			PathStrings.SAVEFILE_NAME);


	}
}
