﻿using ShadertoyScreensaver;
using SharpGL;
using SharpGL.SceneGraph.Assets;
using System;
using static ShadertoyScreensaver.GlslFactory;

namespace ShadertoyScreensaver.Textures
{
	public class Framebuffer : IDisposable
	{
		public uint framebufferID;
		public uint textureID { get { return texture.TextureID; } }
		public int width;
		public int height;

		public Texture2D texture;


		private int[] originalViewport = new int[4];

		protected OpenGL gl;

		public Framebuffer(OpenGL gl, int width, int height)
		{
			this.gl = gl;
			this.width = width;
			this.height = height;

			texture = new Texture2D(gl, width, height);

			// Generate and bind the framebuffer
			uint[] framebuffers = new uint[1];
			gl.GenFramebuffersEXT(1, framebuffers);
			framebufferID = framebuffers[0];
			gl.BindFramebufferEXT(OpenGL.GL_FRAMEBUFFER_EXT, framebufferID);

			texture.BindTexture();
			// Attach the texture to the framebuffer
			gl.FramebufferTexture2DEXT(OpenGL.GL_FRAMEBUFFER_EXT, OpenGL.GL_COLOR_ATTACHMENT0_EXT, OpenGL.GL_TEXTURE_2D, textureID, 0);

			// Check if the framebuffer is complete
			uint status = gl.CheckFramebufferStatusEXT(OpenGL.GL_FRAMEBUFFER_EXT);
			if (status != OpenGL.GL_FRAMEBUFFER_COMPLETE_EXT)
			{
				throw new Exception("Framebuffer is not complete.");
			}

			// Unbind the framebuffer
			gl.BindFramebufferEXT(OpenGL.GL_FRAMEBUFFER_EXT, 0);
		}

		public void Bind()
		{
			gl.GetInteger(OpenGL.GL_VIEWPORT, originalViewport);
			gl.BindFramebufferEXT(OpenGL.GL_FRAMEBUFFER_EXT, framebufferID);
			gl.Viewport(0, 0, width, height);
		}


		public void Unbind()
		{
			texture.GenerateMipmaps();

			gl.BindFramebufferEXT(OpenGL.GL_FRAMEBUFFER_EXT, 0);
			gl.Viewport(originalViewport[0], originalViewport[1], originalViewport[2], originalViewport[3]);
		}

		public void ApplyTextureSettings(FilterMode filterMode, WrapMode wrapMode)
		{
			texture.SetFilterMode(filterMode);
			texture.SetWrapMode(wrapMode);
		}

		public void Dispose()
		{
			texture.Dispose();
		}
	}
}