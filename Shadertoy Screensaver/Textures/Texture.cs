﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace ShadertoyScreensaver.Textures
{
	public abstract class Texture : IDisposable
	{
		protected OpenGL gl;
		public uint TextureID { get; protected set; }
		public WrapMode WrapMode { get; protected set; }
		public FilterMode FilterMode { get; protected set; }

		public abstract string GetShaderUniform();

		public abstract uint GetTextureType();

		public Texture(OpenGL gl)
		{
			this.gl = gl;
			uint[] textures = new uint[1];
			gl.GenTextures(1, textures);
			TextureID = textures[0];
		}

		public void BindTexture()
		{
			gl.BindTexture(GetTextureType(), TextureID);
		}

		public virtual void SetWrapMode(WrapMode wrapMode)
		{
			WrapMode = wrapMode;
			BindTexture();
			gl.TexParameter(GetTextureType(), OpenGL.GL_TEXTURE_WRAP_S, (uint)wrapMode);
			gl.TexParameter(GetTextureType(), OpenGL.GL_TEXTURE_WRAP_T, (uint)wrapMode);
		}

		public virtual void SetFilterMode(FilterMode filterMode)
		{
			FilterMode = filterMode;
			BindTexture();
			gl.TexParameter(GetTextureType(), OpenGL.GL_TEXTURE_MIN_FILTER, (uint)filterMode);
			gl.TexParameter(GetTextureType(), OpenGL.GL_TEXTURE_MAG_FILTER, (uint)filterMode);
			GenerateMipmaps();
		}

		public void GenerateMipmaps()
		{
			if (FilterMode == FilterMode.MIPMAP)
			{
				BindTexture(); 
				gl.GenerateMipmapEXT(GetTextureType()); // Generate mipmaps
			}
		}

		public virtual float GetChannelTime()
		{
			return 0;
		}

		public abstract float[] GetChannelResolution(); //Returns a float[3] x, y, z
		
		public virtual void Update()
		{
			//This gets called before the frame gets drawn. It only does stuff on textures that move/change.
		}

		public virtual void Dispose()
		{
			//This gets called when the object dies
		}
	}
}
