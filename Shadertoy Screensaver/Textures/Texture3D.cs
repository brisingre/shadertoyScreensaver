﻿using ShadertoyScreensaver.API;
using SharpGL;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using System;
using System.Diagnostics;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;

namespace ShadertoyScreensaver.Textures
{
	public class Texture3D : Texture
	{
		public int width { get; private set; }
		public int height { get; private set; }
		public int depth { get; private set; }

		public Texture3D(OpenGL gl) :base(gl)
		{
			
		}

		public Texture3D(OpenGL gl, int width, int height, int depth, WrapMode wrapMode = WrapMode.REPEAT, FilterMode filterMode = FilterMode.MIPMAP) : base(gl)
		{
			this.width	= width;
			this.height = height;
			this.depth = depth;

			BindTexture();
			gl.TexImage3D(GetTextureType(), 0, (int)PixelFormat.RGBA, width, height, depth, 0, (uint)PixelFormat.RGBA, OpenGL.GL_UNSIGNED_BYTE, IntPtr.Zero);
			SetWrapMode(wrapMode);
			SetFilterMode(filterMode);
		}

		public Texture3D(OpenGL gl, byte[] volume, ShaderToyDataSampler sampler) : base(gl)
		{
			var wrapMode = OpenGlEnums.WrapModeFromString(sampler.Wrap);
			var filterMode = OpenGlEnums.FilterModeFromString(sampler.Filter);

			Setup(volume, wrapMode, filterMode);
		}

		public Texture3D(OpenGL gl, byte[] volume, WrapMode wrapMode = WrapMode.REPEAT, FilterMode filterMode=FilterMode.MIPMAP, bool vFlip=true) : base(gl)
		{
			Setup(volume, wrapMode, filterMode);
		}
		
		public void Setup(byte[] volume, WrapMode wrapMode, FilterMode filterMode)
		{
			BindTexture();

			GCHandle pinnedArray = GCHandle.Alloc(volume, GCHandleType.Pinned);
			IntPtr pointer = pinnedArray.AddrOfPinnedObject();
			pointer += 20;

			Debug.WriteLine("3D Texture Header: ");
			for (int i = 0; i < 20; i++)
			{
				Debug.Write($"{volume[i]}, ");
			}

			int width = volume[4];
			int height = volume[8];
			int depth = volume[12];
			int byteWidth = volume[16];

			this.width = width;
			this.height = height;
			this.depth = depth;

			Debug.WriteLine($"Total Length={volume.Length} 3D Texture Byte Width={byteWidth} Dimensions W={width} H={height}, D={depth}");
			PixelFormat pixelFormat;
			switch (byteWidth)
			{
				case 1:
					pixelFormat = PixelFormat.RED;
					break;
				case 3:
					pixelFormat = PixelFormat.RGB;
					break;
				case 4: 
					pixelFormat = PixelFormat.RGBA;
					break;
				default:
					throw new Exception($"Texture3D can't handle a texture with {byteWidth} bytes per pixel");
			}

			gl.TexImage3D(GetTextureType(), 0, (int)pixelFormat, width, height, depth, 0, (uint)pixelFormat, OpenGL.GL_UNSIGNED_BYTE, pointer);

			pinnedArray.Free();

			SetWrapMode(wrapMode);
			SetFilterMode(filterMode);
		}

		public override uint GetTextureType()
		{
			return OpenGL.GL_TEXTURE_3D;
		}

		public override string GetShaderUniform()
		{
			return GlslSnippets.UNIFORM_SAMPLER_3D;
		}

		public override void SetWrapMode(WrapMode wrapMode)
		{
			base.SetWrapMode(wrapMode);

			gl.TexParameter(GetTextureType(), OpenGL.GL_TEXTURE_WRAP_R, (uint)wrapMode);
		}

		public override float[] GetChannelResolution()
		{
			return new float[]{ width, height, depth };
		}
	}
}
