﻿using SharpGL;
using ShadertoyScreensaver.API;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp;
using System;
using SixLabors.ImageSharp.Processing;
using System.Diagnostics;

namespace ShadertoyScreensaver.Textures
{
	public class Cubemap : Texture
	{
		public Cubemap(OpenGL gl) : base(gl)
		{

		}

		public Cubemap(OpenGL gl, Image<Rgba32> image, ShaderToyDataSampler sampler) : base(gl)
		{
            bool vFlip = Helpers.BoolFromString(sampler.Vflip);
			var wrapMode = OpenGlEnums.WrapModeFromString(sampler.Wrap);
			var filterMode = OpenGlEnums.FilterModeFromString(sampler.Filter);
			Setup(image, wrapMode, filterMode, vFlip);
		}

		public Cubemap(OpenGL gl, Image<Rgba32> image, WrapMode wrapMode = WrapMode.CLAMP, FilterMode filterMode = FilterMode.MIPMAP, bool vFlip = true) : base(gl)
		{
			Setup(image, wrapMode, filterMode, vFlip);
		}

		public void Setup(Image<Rgba32> verticalStripImage, WrapMode wrapMode = WrapMode.CLAMP, FilterMode filterMode = FilterMode.MIPMAP, bool vFlip = true)
		{
			BindTexture();

			var faces = SplitVerticalStrip(verticalStripImage);
			for (int i = 0; i < 6; i++)
			{
				var tex = faces[i];

				byte[] pixels = new byte[tex.Width * tex.Height * 4]; // 4 bytes per pixel (RGBA)
				tex.CopyPixelDataTo(pixels);

				gl.TexImage2D((uint)(OpenGL.GL_TEXTURE_CUBE_MAP_POSITIVE_X + i), 0, OpenGL.GL_RGBA, tex.Width, tex.Height, 0, OpenGL.GL_RGBA, OpenGL.GL_UNSIGNED_BYTE, pixels);

				//gl.TexImage2D((uint)(OpenGL.GL_TEXTURE_CUBE_MAP_POSITIVE_X + i), 0, OpenGL.GL_RGB, data.Width, data.Height, 0, OpenGL.GL_BGR, OpenGL.GL_UNSIGNED_BYTE, data.Scan0);
			}

			SetFilterMode(filterMode);
			SetWrapMode(wrapMode);

			foreach (var face in faces)
				face.Dispose();
		}

		public override uint GetTextureType()
		{
			return OpenGL.GL_TEXTURE_CUBE_MAP;
		}

		public override string GetShaderUniform()
		{
			return GlslSnippets.UNIFORM_SAMPLER_CUBE;
		}

		Image<Rgba32>[] SplitVerticalStrip(Image<Rgba32> verticalStrip)
		{
			int width = verticalStrip.Width;
			int height = verticalStrip.Height / 6;

			// Create an array to hold the cubemap faces
			Image<Rgba32>[] faces = new Image<Rgba32>[6];
			// Calculate the height of each smaller image
			int numberOfSmallerImages = 6;

			int smallerImageHeight = verticalStrip.Height / numberOfSmallerImages;
			int smallerImageWidth = verticalStrip.Width;

			for (int i = 0; i < numberOfSmallerImages; i++)
			{
				// Define the rectangle area to crop for each smaller image
				Rectangle cropArea = new Rectangle(0, i * smallerImageHeight, smallerImageWidth, smallerImageHeight);

				// Crop the smaller image
				var clone = verticalStrip.Clone();
				clone.Mutate(ctx => ctx.Crop(cropArea));

				faces[i] = clone;
				// Save the smaller image
				string outputImagePath = $"face_{i}.png";
				clone.Save(outputImagePath);

				Debug.WriteLine($"Smaller image {i + 1} saved to {outputImagePath}");
			}
			return faces;
		}

		public override void SetWrapMode(WrapMode wrapMode)
		{
			BindTexture();
			gl.TexParameter(OpenGL.GL_TEXTURE_CUBE_MAP, OpenGL.GL_TEXTURE_WRAP_S, (uint)wrapMode);
			gl.TexParameter(OpenGL.GL_TEXTURE_CUBE_MAP, OpenGL.GL_TEXTURE_WRAP_T, (uint)wrapMode);
			gl.TexParameter(OpenGL.GL_TEXTURE_CUBE_MAP, OpenGL.GL_TEXTURE_WRAP_R, (uint)wrapMode);
		}

		public override float[] GetChannelResolution()
		{
			return new float[] { 0, 0, 0 }; //Bafflingly shadertoy just puts zeros in this.
		}
	}
}
