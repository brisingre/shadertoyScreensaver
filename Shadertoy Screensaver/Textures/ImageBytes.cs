﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text;

namespace ShadertoyScreensaver.Textures
{
	public class ImageBytes
	{
		public class Format
		{
			public static readonly Format RGBA = new Format(0, 1, 2, 3);
			public static readonly Format ARGB = new Format(3, 0, 1, 2);
			public static readonly Format RGB = new Format(0, 1, 2);
			public static readonly Format R = new Format(0);

			public int[] channels { get; private set; }
			public int[] indexOfChannels { get; private set; } = new int[] { -1, -1, -1, -1 };
			public int width = 0;

			public Format(params int[] channels) {
				this.channels = channels;
				for (int i = 0; i < channels.Length; i++) {
					indexOfChannels[channels[i]] = i;
				}
				width = channels.Length;
			}
		}
		
		public byte[] data;

		public int width;
		public int height;

		public int pixelCount { get { return width * height; } }

		public Format format { get; private set; }


		public void FromBitmap(Bitmap bitmap)
		{
			width = bitmap.Width;
			height = bitmap.Height;

			Debug.WriteLine(bitmap.PixelFormat);

			BitmapData bitmapData = bitmap.LockBits(new Rectangle(0, 0, bitmap.Width, bitmap.Height),
										ImageLockMode.ReadOnly, bitmap.PixelFormat);

			switch(bitmap.PixelFormat)
			{
				case System.Drawing.Imaging.PixelFormat.Format24bppRgb:
					format = Format.RGB;
					break;
				case System.Drawing.Imaging.PixelFormat.Format32bppArgb:
					format = Format.ARGB;
					break;
				default:
					throw new Exception($"ImageBytes doesn't support PixelFormat: {bitmap.PixelFormat}");
			}

			int byteCount = bitmapData.Stride * bitmap.Height;
			data = new byte[byteCount];
			System.Runtime.InteropServices.Marshal.Copy(bitmapData.Scan0, data, 0, byteCount);


			bitmap.UnlockBits(bitmapData);
			Reformat(Format.R);
			Reformat(Format.ARGB);
		}


		Func<int, int> BuildMapperFunction(Format oldFormat, Format newFormat)
		{
			Func<int, int> mapper = delegate (int positionInNewFormat)
			{
				//Input is our byte position in the new format
				//Fail if input is not 0-newFormat.width

				//Get what channel belongs in that position from the new format
				var channel = newFormat.channels[positionInNewFormat];

				//Get what channel has that in the old format;
				var oldChannel = oldFormat.indexOfChannels[channel];

				return oldChannel;

			};
			return mapper;
		}


		void Reformat(Format newFormat)
		{
			var mapper = BuildMapperFunction(format, newFormat);
			byte[] newData = new byte[pixelCount * newFormat.width];
			for (int i = 0; i < pixelCount; i++)
			{
				var startOld = i * format.width;
				var startNew = i * newFormat.width;

				for(int j = 0; j < newFormat.width; j++) 
				{
					var offset = mapper(j);
					if (offset >= 0)
						newData[startNew + j] = data[startOld + offset];
					else
						newData[startNew + j] = 0;
				}
			}
			format = newFormat;
			data = newData;
		}
	}
}
