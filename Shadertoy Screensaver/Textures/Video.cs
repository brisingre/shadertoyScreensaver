﻿using SharpGL;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LibVLCSharp.Shared;
using ShadertoyScreensaver.API;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.Window;
using System.Diagnostics;
using System.Security.Cryptography;
using System.Windows.Forms.Design;
using MediaToolkit;

namespace ShadertoyScreensaver.Textures
{
	internal class Video : Texture2D
	{
		public Video(OpenGL gl) : base(gl)
		{

		}

		private LibVLC libvlc; //Not properly disposed of

		public MediaPlayer mediaPlayer; //Not properly disposed of

		public Media media;

		bool frameBytes1IsFront;

		byte[] frameBytes1;
		byte[] frameBytes2;





		public byte[] backFrameBytes { 
			get { return frameBytes1IsFront ? frameBytes2 : frameBytes1; }	 
			private set {
				if (frameBytes1IsFront)
					frameBytes2 = value;
				else frameBytes1 = value;
			} }

		public byte[] frontFrameBytes
		{
			get { return frameBytes1IsFront ? frameBytes1 : frameBytes2; }
		}

		#region Texture stuff
		public Video(OpenGL gl, string path, ShaderToyDataSampler sampler, bool mute = true): base(gl, sampler, PixelFormat.RGB)
		{

			//The files shadertoy provides for this don't have the resolution the website says they do.
			//These values are from the website.
			/*var filename = Path.GetFileNameWithoutExtension(path);
			switch (filename)
			{
				case "e81e818ac76a8983d746784b423178ee9f6cdcdf7f8e8d719341a6fe2d2ab303": //Britney Spears
					width = 352;
					height = 288;
					break;
				case "35c87bcb8d7af24c54d41122dadb619dd920646a0bd0e477e7bdc6d12876df17": //Claude Van Damme
					width = 640;
					height = 486;
					break;
				case "3405e48f74815c7baa49133bdc835142948381fbe003ad2f12f5087715731153": //1961 Commercial
					width = 640;
					height = 480;
					break;
				case "c3a071ecf273428bc72fc72b2dd972671de8da420a2d4f917b75d20e1c24b34c": //Google Logo
					width = 640;
					height = 320;
					break;
				default:
					width = 256;
					height = 256;
					break;
			}*/


			var inputFile = new MediaToolkit.Model.MediaFile { Filename = path };
			using (var engine = new Engine())
			{
				engine.GetMetadata(inputFile);
			}
			string[] dimensions = inputFile.Metadata.VideoData.FrameSize.Split('x');

			if (dimensions.Length == 2 && int.TryParse(dimensions[0], out int videoWidth) && int.TryParse(dimensions[1], out int videoHeight))
			{
				Console.WriteLine($"Video Width: {videoWidth} pixels");
				Console.WriteLine($"Video Height: {videoHeight} pixels");
				width = videoWidth;
				height = videoHeight;
			}
			else
			{
				Console.WriteLine("Unable to determine video dimensions.");
			}




			bool vFlip = Helpers.BoolFromString(sampler.Vflip);



			string[] libVlcOptions = new string[] { };
			if(vFlip)
			{
				libVlcOptions = new string[]{"--video-filter=transform", "--transform-type=vflip"};
			}
			libvlc = new LibVLC(libVlcOptions);
			libvlc.Log += OnLogMessage;


			media = new Media(libvlc, path, FromType.FromPath);


			//media.ParsedChanged += (sender, eventArgs) =>
			//{
			//	if (media.IsParsed)
			//	{
			//		// Now you can get the video track information
			//		var track = media.Tracks.FirstOrDefault(track => track.TrackType == TrackType.Video);

			//		// Print or use the resolution
			//		Debug.WriteLine($"Video resolution: {track.Data.Video.Width}x{track.Data.Video.Height}");

			//	}

			//	/*if (eventArgs.ParsedStatus == MediaParsedStatus.Done)
			//	{
			//		Debug.WriteLine($"ParsedChange Callback! Track Count = {media.Tracks.Length}");
			//		var videoTrack = media.Tracks[0].Data.Video;
			//		//width = (int)videoTrack.Width;
			//		//height = (int)videoTrack.Height;
			//		Debug.WriteLine($"Width: {videoTrack.Width}, Height: {videoTrack.Height}, FrameRate: ({videoTrack.FrameRateNum}/{videoTrack.FrameRateDen})");
			//	}*/
			//};

			media.Parse(MediaParseOptions.ParseNetwork);

			mediaPlayer = new MediaPlayer(libvlc);
			mediaPlayer.SetVideoFormat("RV32", (uint)width, (uint)height, (uint)width * 4);
			//mediaPlayer.SetVideoFormatCallbacks(VideoFormat, null);
			mediaPlayer.Playing += (sender, eventArgs) =>
			{
			};

			/*mediaPlayer.PositionChanged += (sender, eventArgs) =>
			{
				uint w = 0;
				uint h = 0;
				mediaPlayer.Size(0, ref w, ref h);

				if (width != w || height != h)
				{
					width = (int)w;
					height = (int)h;
				}
				Debug.WriteLine($"Video Width: {w}, Video Height: {h}");

			};*/

			mediaPlayer.EndReached += (sender, e) =>
			{
				mediaPlayer.Play();
			};


			mediaPlayer.Media = media;
			
			if (mute)
				mediaPlayer.Volume = 0;

			mediaPlayer.Play();

			mediaPlayer.SetVideoCallbacks(Lock, Unlock, Display);




		}

		public override void Update()
		{
			base.Update();
		
			SetBytes(frontFrameBytes, width, height, PixelFormat.RGBA);
			GenerateMipmaps();
		}

		public override async void Dispose()
		{

			//Task.Run(() =>
			//{
				if (media != null)
				{
					media.Dispose();
					media = null;
				}

				if (mediaPlayer != null)
				{
					if (mediaPlayer.IsPlaying)
					{
						await Task.Run(() => mediaPlayer.Stop());
					}
					
					mediaPlayer.Dispose();
					mediaPlayer = null;
				}

				if (libvlc != null)
				{
					libvlc.Dispose();
					libvlc = null;
				}
			//}).Wait();



		}

		public override float GetChannelTime()
		{
			return (float)mediaPlayer.Time / 1000f;
		}

		#endregion

		#region VLC stuff
		private IntPtr Lock(IntPtr opaque, IntPtr planes)
		{
			// Allocate memory for the frame
			IntPtr buffer = System.Runtime.InteropServices.Marshal.AllocHGlobal(width * height * 4);
			System.Runtime.InteropServices.Marshal.WriteIntPtr(planes, buffer);
			return buffer;
		}

		private void Unlock(IntPtr opaque, IntPtr picture, IntPtr planes)
		{
			// Copy the frame data
			IntPtr buffer = System.Runtime.InteropServices.Marshal.ReadIntPtr(planes);
			backFrameBytes = new byte[width * height * 4];
			System.Runtime.InteropServices.Marshal.Copy(buffer, backFrameBytes, 0, backFrameBytes.Length);

			// Free the buffer memory
			System.Runtime.InteropServices.Marshal.FreeHGlobal(buffer);



			var rng = new Random();
			//rng.NextBytes(backBuffer);

			BgraToRgba(backFrameBytes);
			frameBytes1IsFront = !frameBytes1IsFront;

		}

		private void Display(IntPtr opaque, IntPtr picture)
		{
			// Display the frame if needed
		}

		private void OnLogMessage(object sender, LogEventArgs e)
		{
			if(e.Level == LogLevel.Error)
				Debug.WriteLine($"[{e.Level}] {e.Module}: {e.Message}");
		}

		/*private uint VideoFormat(ref IntPtr opaque, IntPtr chroma, ref uint width, ref uint height, ref uint pitches, ref uint lines)
		{
			// Set the pixel format to RGBA32 (32-bit RGBA)
			string format = "RV32"; // 32-bit RGBA

			// Copy the format to the chroma pointer
			byte[] formatBytes = System.Text.Encoding.ASCII.GetBytes(format);
			System.Runtime.InteropServices.Marshal.Copy(formatBytes, 0, chroma, formatBytes.Length);

			// Set the width and height
			width = (uint)this.width;
			height = (uint)this.height;

			// Set the pitches (bytes per line) and lines (number of lines)
			pitches = width * 4; // 4 bytes per pixel for RGBA
			lines = height;

			return 1;
		}*/
		#endregion

		#region Helpers
		void BgraToRgba(byte[] bytes)
		{
			int stride = 4; //4 bytes per color in RGBA and BGRA
			byte swap;
			for(int i = 0; i < bytes.Length; i += stride)
			{
				swap  = bytes[i];
				bytes[i] = bytes[i + 2];
				bytes[i+2] = swap; 
			}
		}
		#endregion
	}
}
