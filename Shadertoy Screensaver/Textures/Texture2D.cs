﻿using ShadertoyScreensaver.API;
using SharpGL;
using SharpGL.SceneGraph.Lighting;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using System;
using System.Diagnostics;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;

namespace ShadertoyScreensaver.Textures
{
	public class Texture2D : Texture
	{
		public int width { get; protected set; }
		public int height { get; protected set; }


		public Texture2D(OpenGL gl) :base(gl)
		{
			
		}

		public Texture2D(OpenGL gl, int width, int height, WrapMode wrapMode = WrapMode.REPEAT, FilterMode filterMode = FilterMode.MIPMAP, PixelFormat pixelFormat = PixelFormat.RGBA) : base(gl)
		{
			this.width = width;
			this.height = height;

			BindTexture();

			gl.TexImage2D(GetTextureType(), 0, (uint)pixelFormat, width, height, 0,
						  (uint)pixelFormat, OpenGL.GL_UNSIGNED_BYTE, IntPtr.Zero);
			SetWrapMode(wrapMode);
			SetFilterMode(filterMode);
		}

		public Texture2D(OpenGL gl, Image image, ShaderToyDataSampler sampler) : base(gl)
		{
			bool vFlip = Helpers.BoolFromString(sampler.Vflip);
			var wrapMode = OpenGlEnums.WrapModeFromString(sampler.Wrap);
			var filterMode = OpenGlEnums.FilterModeFromString(sampler.Filter);

			SetupImage(image, vFlip);
			SetWrapMode(wrapMode);
			SetFilterMode(filterMode);
		}



		public Texture2D(OpenGL gl, ShaderToyDataSampler sampler, PixelFormat pixelFormat = PixelFormat.RGBA) : base(gl)
		{
			var wrapMode = OpenGlEnums.WrapModeFromString(sampler.Wrap);
			var filterMode = OpenGlEnums.FilterModeFromString(sampler.Filter);

			BindTexture();
			gl.TexImage2D(GetTextureType(), 0, (uint)pixelFormat, 16, 16, 0,
						  (uint)pixelFormat, OpenGL.GL_UNSIGNED_BYTE, IntPtr.Zero);

			SetWrapMode(wrapMode);
			SetFilterMode(filterMode);
		}

		public Texture2D(OpenGL gl, Image image, WrapMode wrapMode = WrapMode.REPEAT, FilterMode filterMode=FilterMode.MIPMAP, bool vFlip=true) : base(gl)
		{
			SetupImage(image, vFlip);
			SetWrapMode(wrapMode);
			SetFilterMode(filterMode);
		}




		public void SetupImage(Image image, bool vFlip)
		{
			if (vFlip)
				image.Mutate(x => x.Flip(FlipMode.Vertical));

			this.width = image.Width;
			this.height = image.Height;

			byte[] pixels;
			PixelFormat format = PixelFormat.RGBA;

			if (image is Image<Rgba32>)
			{
				pixels = new byte[image.Width * image.Height * 4]; // 4 bytes per pixel (RGBA)
				((Image<Rgba32>)image).CopyPixelDataTo(pixels);
				format = PixelFormat.RGBA;
			}
			else if (image is Image<Rgb24>)
			{
				pixels = new byte[image.Width * image.Height * 3]; // 3 bytes per pixel (RGB)
				((Image<Rgb24>)image).CopyPixelDataTo(pixels);
				format = PixelFormat.RGB;
			}
			else if (image is Image<L8>)
			{
				pixels = new byte[image.Width * image.Height * 1]; // 1 bytes per pixel (RED)
				((Image<L8>)image).CopyPixelDataTo(pixels);
				format = PixelFormat.RED;
			}
			else
			{
				throw new Exception($"Texture2D is not prepared for an image of PixelType {image.PixelType}.");
			}

			BindTexture();
			gl.TexImage2D(GetTextureType(), 0, (uint)format, image.Width, image.Height, 0,
						  (uint)format, OpenGL.GL_UNSIGNED_BYTE, pixels);

		}

		public void SetBytes(byte[] bytes, int width, int height, PixelFormat format = PixelFormat.RGBA)
		{
			this.width = width;
			this.height = height;
			BindTexture();

			gl.TexImage2D(GetTextureType(), 0, (uint)format, width, height, 0,
						  (uint)format, OpenGL.GL_UNSIGNED_BYTE, bytes);
		}


		public override uint GetTextureType()
		{
			return OpenGL.GL_TEXTURE_2D;
		}

		public override string GetShaderUniform()
		{
			return GlslSnippets.UNIFORM_SAMPLER_2D;
		}

		public override float[] GetChannelResolution()
		{
			return new float[] { width, height, 1 };
		}
	}
}
