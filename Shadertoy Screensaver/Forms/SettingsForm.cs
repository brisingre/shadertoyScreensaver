﻿using Microsoft.Win32;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using ShadertoyScreensaver.API;
using LibVLCSharp.Shared;
using Microsoft.VisualBasic.Devices;
using Newtonsoft.Json;
using ShadertoyScreensaver.Config;

namespace ShadertoyScreensaver
{
	public partial class SettingsForm : Form
	{
		public SettingsForm()
		{
			InitializeComponent();
			LoadSettings();
		}

		private void okButton_Click(object sender, EventArgs e)
		{
			SaveSettings();
			Close();
		}

		private void cancelButton_Click(object sender, EventArgs e)
		{
			RevertSettings();
			Close();
		}

		ShadertoyScreensaverConfig PullConfig()
		{
			return new ShadertoyScreensaverConfig()
			{
				FrameRate = (int)frameRateUpDown.Value,
				Fullscreen = checkBoxFullscreen.Checked,
				ShadertoyURL = urlTextInput.Text,
				KeyboardInput = keyboardCheckBox.Checked,
				MouseInput = mouseCheckBox.Checked,
			};
		}

		void PushConfig(ShadertoyScreensaverConfig config)
		{
			frameRateUpDown.Value = config.FrameRate;
			checkBoxFullscreen.Checked = config.Fullscreen;
			urlTextInput.Text = config.ShadertoyURL;
			keyboardCheckBox.Checked = config.KeyboardInput;
			mouseCheckBox.Checked = config.MouseInput;
		}

		void RevertSettings()
		{
			var json = JsonConvert.SerializeObject(cachedConfig, Formatting.Indented);
			File.WriteAllText(ShadertoyScreensaverConfig.SavePath, json);
		}

		void SaveSettings()
		{
			var json = JsonConvert.SerializeObject(PullConfig(), Formatting.Indented);
			File.WriteAllText(ShadertoyScreensaverConfig.SavePath, json);
		}

		ShadertoyScreensaverConfig cachedConfig;
		void LoadSettings()
		{
			cachedConfig = Helpers.LoadConfig();
			PushConfig(cachedConfig);
		}



		private void SettingsForm_Load(object sender, EventArgs e)
		{

		}



		private void editButton_Click(object sender, EventArgs e)
		{
			var file = "Shaders\\Shadertoy\\" + fontComboBox.SelectedItem + ".glsl";
			OpenWithDefaultProgram(file);
		}

		public static void OpenWithDefaultProgram(string path)
		{
			using Process fileopener = new Process();

			fileopener.StartInfo.FileName = "explorer";
			fileopener.StartInfo.Arguments = "\"" + path + "\"";
			fileopener.Start();
		}

		private void previewButton_Click(object sender, EventArgs e)
		{
			SaveSettings();
			Program.ShowScreenSaver();
		}

		private async void testApiButton_Click(object sender, EventArgs e)
		{
			Debug.WriteLine("Hit a button nerd");

			var path = """K:\newgit\shadertoyScreensaver\Shadertoy Screensaver\bin\Debug\net7.0-windows\Assets\3405e48f74815c7baa49133bdc835142948381fbe003ad2f12f5087715731153.ogv""";


			var libvlc = new LibVLC();
			var mediaPlayer = new MediaPlayer(libvlc);

			var media = new Media(libvlc, path, FromType.FromPath);

			mediaPlayer.Play(media);

			//mediaPlayer.Dispose();
			//libvlc.Dispose();

			/*try
			{
				var shaderToyAPI = new ShaderToyAPI(Program.API_KEY);
				Debug.WriteLine(shaderToyAPI);
				// Call the method to fetch shaders
				var result = await shaderToyAPI.FetchShaderAndAssetsFromIDAsync(urlTextInput.Text);

				Debug.WriteLine("FetchShadersAsync completed");
				Debug.WriteLine(result.ShaderToyDataContainer.Shader.RenderPass[0].Code);
				Debug.WriteLine(result.ShaderToyAssets.Keys.ToList().ToString());
			}
			catch (Exception ex)
			{
				Debug.WriteLine("Failed to fetch data: " + ex.Message);
			}*/
		}

		private void label2_Click(object sender, EventArgs e)
		{

		}

		private void checkBoxFullscreen_CheckedChanged(object sender, EventArgs e)
		{

		}
	}
}
