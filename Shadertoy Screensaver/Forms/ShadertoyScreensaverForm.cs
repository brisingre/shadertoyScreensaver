﻿using Microsoft.Win32;
using ModernOpenGLSample;
using SharpGL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ScreensaverExample
{
	public partial class ShadertoyScreensaverForm : Form
	{
		[DllImport("user32.dll")]
		static extern IntPtr SetParent(IntPtr hWndChild, IntPtr hWndNewParent);
		[DllImport("user32.dll")]
		static extern int SetWindowLong(IntPtr hWind, int nIndex, IntPtr dwNewLong);
		[DllImport("user32.dll")]
		static extern int GetWindowLong(IntPtr hWind, int nIndex);
		[DllImport("user32.dll")]
		static extern bool GetClientRect(IntPtr hWind, out Rectangle lpRect);

		private Point mousePoint;
		private Random rand = new Random();
		private bool previewMode = false;

		//Constructor for preview mode
		public ShadertoyScreensaverForm(IntPtr previewWndHandle)
		{
			InitializeComponent();

			//Set The preview window as the parent of this window
			SetParent(this.Handle, previewWndHandle);

			//Make this a child window so it will close when the parent closes
			//GWL_STYLE = -16, WS_CHILD = 0x40000000
			SetWindowLong(this.Handle, -16, new IntPtr(GetWindowLong(this.Handle, -16) | 0x40000000));

			//Place our window inside the parent
			Rectangle parentRect;
			GetClientRect(previewWndHandle, out parentRect);
			Size = parentRect.Size;
			Location = new Point(0, 0);

			previewMode = true;
			Setup(1f/12f);
		}
		//Constructor for fullscreen mode
		public ShadertoyScreensaverForm(Rectangle bounds)
		{
			InitializeComponent();
			this.Bounds = bounds;
			Setup();
		}

		private void Setup(float fontScale = 1)
		{
			RegistryKey key = Registry.CurrentUser.OpenSubKey("SOFTWARE\\ScreensaverExample");
			if (key == null)
			{
				textLabel.Text = "Seagull Incident";
				textLabel.ForeColor = Color.Goldenrod;
				textLabel.BackColor = BackColor = Color.Aquamarine;
				moveTimer.Interval = 1000;
				textLabel.Font = new Font("Segoe Script", 72 * fontScale);
			}
			else
			{
				textLabel.Text = (string)key.GetValue("text");
				textLabel.ForeColor = Color.FromArgb((int)key.GetValue("textColor"));
				textLabel.BackColor = BackColor = Color.FromArgb((int)key.GetValue("backgroundColor"));
				moveTimer.Interval = (int)(Decimal.Parse((string)key.GetValue("speed")) * 1000);

				var font = (string)key.GetValue("font");
				var fontSize = (float)(Decimal.Parse((string)key.GetValue("fontSize")));
				textLabel.Font = new Font(font, fontSize * fontScale);
			}



			moveTimer.Start();
			RandomMove();
		}

		private void ScreenSaverForm_Load(object sender, EventArgs e)
		{
			Cursor.Hide();
			TopMost = true;
		}

		private void ScreenSaverForm_MouseClick(object sender, MouseEventArgs e)
		{
			if(!previewMode)
				Application.Exit();
		}

		private void ScreenSaverForm_KeyPress(object sender, KeyPressEventArgs e)
		{
			if(!previewMode)
				Application.Exit();
		}

		private void ScreenSaverForm_MouseMove(object sender, MouseEventArgs e)
		{
			float sensitivity = 5;
			if(!mousePoint.IsEmpty)
			{
				if (Math.Abs(mousePoint.X - e.X) > sensitivity || Math.Abs(mousePoint.Y - e.Y) > sensitivity)
					if (!previewMode)
						Application.Exit();
			}
			mousePoint = e.Location;
		}

		private void moveTimer_Tick(object sender, EventArgs e)
		{
			RandomMove();
		}

		private void RandomMove()
		{
			textLabel.Left = rand.Next(Math.Max(1, Bounds.Width - textLabel.Width));
			textLabel.Top = rand.Next(Math.Max(1, Bounds.Height - textLabel.Height));
		}


		/// <summary>
		/// Handles the OpenGLInitialized event of the openGLControl1 control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
		private void openGLControl1_OpenGLInitialized(object sender, EventArgs e)
		{
			//  Initialise the scene.
			scene.Initialise(openGLControl1.OpenGL, Width, Height);
		}

		/// <summary>
		/// Handles the OpenGLDraw event of the openGLControl1 control.
		/// </summary>
		/// <param name="sender">The source of the event.</param>
		/// <param name="args">The <see cref="RenderEventArgs"/> instance containing the event data.</param>
		private void openGLControl1_OpenGLDraw(object sender, RenderEventArgs args)
		{
			//  Draw the scene.
			scene.Draw(openGLControl1.OpenGL, Width, Height);
		}

		/// <summary>
		/// The scene that we are rendering.
		/// </summary>
		private readonly Scene scene = new Scene();

	}
}
