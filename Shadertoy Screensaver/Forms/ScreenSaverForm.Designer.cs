﻿
namespace ShadertoyScreensaver
{
	partial class ScreenSaverForm
	{
		/// <summary>
		///  Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		///  Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		///  Required method for Designer support - do not modify
		///  the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			textLabel = new System.Windows.Forms.Label();
			openGLControl1 = new SharpGL.OpenGLControl();
			((System.ComponentModel.ISupportInitialize)openGLControl1).BeginInit();
			SuspendLayout();
			// 
			// textLabel
			// 
			textLabel.AutoSize = true;
			textLabel.BackColor = System.Drawing.Color.Teal;
			textLabel.Dock = System.Windows.Forms.DockStyle.Left;
			textLabel.Font = new System.Drawing.Font("Nunito", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
			textLabel.ForeColor = System.Drawing.Color.FromArgb(192, 255, 255);
			textLabel.Location = new System.Drawing.Point(0, 0);
			textLabel.Name = "textLabel";
			textLabel.Size = new System.Drawing.Size(108, 19);
			textLabel.TabIndex = 1;
			textLabel.Text = "Seagull Incident";
			textLabel.Visible = false;
			// 
			// openGLControl1
			// 
			openGLControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			openGLControl1.DrawFPS = false;
			openGLControl1.ForeColor = System.Drawing.SystemColors.ControlText;
			openGLControl1.FrameRate = 60;
			openGLControl1.Location = new System.Drawing.Point(0, 0);
			openGLControl1.Margin = new System.Windows.Forms.Padding(0);
			openGLControl1.Name = "openGLControl1";
			openGLControl1.OpenGLVersion = SharpGL.Version.OpenGLVersion.OpenGL4_4;
			openGLControl1.RenderContextType = SharpGL.RenderContextType.NativeWindow;
			openGLControl1.RenderTrigger = SharpGL.RenderTrigger.TimerBased;
			openGLControl1.Size = new System.Drawing.Size(800, 450);
			openGLControl1.TabIndex = 2;
			openGLControl1.OpenGLInitialized += openGLControl1_OpenGLInitialized;
			openGLControl1.OpenGLDraw += openGLControl1_OpenGLDraw;
			openGLControl1.KeyPress += openGLControl1_KeyPress;
			openGLControl1.MouseClick += ScreenSaverForm_MouseClick;
			openGLControl1.MouseDown += openGLControl1_MouseDown;
			openGLControl1.MouseMove += ScreenSaverForm_MouseMove;
			openGLControl1.MouseUp += openGLControl1_MouseUp;
			// 
			// ScreenSaverForm
			// 
			AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
			AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			BackColor = System.Drawing.Color.Teal;
			ClientSize = new System.Drawing.Size(800, 450);
			Controls.Add(textLabel);
			Controls.Add(openGLControl1);
			FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			Name = "ScreenSaverForm";
			StartPosition = System.Windows.Forms.FormStartPosition.Manual;
			Text = "Form1";
			Load += ScreenSaverForm_Load;
			MouseClick += ScreenSaverForm_MouseClick;
			MouseMove += ScreenSaverForm_MouseMove;
			((System.ComponentModel.ISupportInitialize)openGLControl1).EndInit();
			ResumeLayout(false);
			PerformLayout();
		}

		#endregion
		private System.Windows.Forms.Label textLabel;
		private SharpGL.OpenGLControl openGLControl1;


	}
}

