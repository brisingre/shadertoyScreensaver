﻿
namespace ShadertoyScreensaver
{
	partial class SettingsForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			okButton = new System.Windows.Forms.Button();
			cancelButton = new System.Windows.Forms.Button();
			label1 = new System.Windows.Forms.Label();
			label5 = new System.Windows.Forms.Label();
			frameRateUpDown = new System.Windows.Forms.NumericUpDown();
			fontComboBox = new System.Windows.Forms.ComboBox();
			editButton = new System.Windows.Forms.Button();
			previewButton = new System.Windows.Forms.Button();
			label2 = new System.Windows.Forms.Label();
			testApiButton = new System.Windows.Forms.Button();
			urlTextInput = new System.Windows.Forms.TextBox();
			performanceBox = new System.Windows.Forms.GroupBox();
			debugBox = new System.Windows.Forms.GroupBox();
			checkBoxFullscreen = new System.Windows.Forms.CheckBox();
			mouseCheckBox = new System.Windows.Forms.CheckBox();
			keyboardCheckBox = new System.Windows.Forms.CheckBox();
			((System.ComponentModel.ISupportInitialize)frameRateUpDown).BeginInit();
			performanceBox.SuspendLayout();
			debugBox.SuspendLayout();
			SuspendLayout();
			// 
			// okButton
			// 
			okButton.Location = new System.Drawing.Point(92, 56);
			okButton.Name = "okButton";
			okButton.Size = new System.Drawing.Size(75, 23);
			okButton.TabIndex = 0;
			okButton.Text = "OK";
			okButton.UseVisualStyleBackColor = true;
			okButton.Click += okButton_Click;
			// 
			// cancelButton
			// 
			cancelButton.Location = new System.Drawing.Point(173, 56);
			cancelButton.Name = "cancelButton";
			cancelButton.Size = new System.Drawing.Size(75, 23);
			cancelButton.TabIndex = 1;
			cancelButton.Text = "Cancel";
			cancelButton.UseVisualStyleBackColor = true;
			cancelButton.Click += cancelButton_Click;
			// 
			// label1
			// 
			label1.AutoSize = true;
			label1.Location = new System.Drawing.Point(6, 48);
			label1.Name = "label1";
			label1.Size = new System.Drawing.Size(43, 15);
			label1.TabIndex = 3;
			label1.Text = "Shader";
			// 
			// label5
			// 
			label5.AutoSize = true;
			label5.Location = new System.Drawing.Point(6, 19);
			label5.Name = "label5";
			label5.Size = new System.Drawing.Size(56, 15);
			label5.TabIndex = 12;
			label5.Text = "FPS Limit";
			// 
			// frameRateUpDown
			// 
			frameRateUpDown.Location = new System.Drawing.Point(6, 37);
			frameRateUpDown.Maximum = new decimal(new int[] { 999, 0, 0, 0 });
			frameRateUpDown.Minimum = new decimal(new int[] { 2, 0, 0, 0 });
			frameRateUpDown.Name = "frameRateUpDown";
			frameRateUpDown.Size = new System.Drawing.Size(75, 23);
			frameRateUpDown.TabIndex = 11;
			frameRateUpDown.Value = new decimal(new int[] { 60, 0, 0, 0 });
			// 
			// fontComboBox
			// 
			fontComboBox.FormattingEnabled = true;
			fontComboBox.Location = new System.Drawing.Point(6, 66);
			fontComboBox.Name = "fontComboBox";
			fontComboBox.Size = new System.Drawing.Size(124, 23);
			fontComboBox.TabIndex = 13;
			// 
			// editButton
			// 
			editButton.Location = new System.Drawing.Point(86, 22);
			editButton.Name = "editButton";
			editButton.Size = new System.Drawing.Size(75, 23);
			editButton.TabIndex = 16;
			editButton.Text = "Edit";
			editButton.UseVisualStyleBackColor = true;
			editButton.Click += editButton_Click;
			// 
			// previewButton
			// 
			previewButton.Location = new System.Drawing.Point(12, 56);
			previewButton.Name = "previewButton";
			previewButton.Size = new System.Drawing.Size(75, 23);
			previewButton.TabIndex = 17;
			previewButton.Text = "Preview";
			previewButton.UseVisualStyleBackColor = true;
			previewButton.Click += previewButton_Click;
			// 
			// label2
			// 
			label2.AutoSize = true;
			label2.Location = new System.Drawing.Point(12, 9);
			label2.Name = "label2";
			label2.Size = new System.Drawing.Size(84, 15);
			label2.TabIndex = 18;
			label2.Text = "Shadertoy URL";
			label2.Click += label2_Click;
			// 
			// testApiButton
			// 
			testApiButton.Location = new System.Drawing.Point(6, 22);
			testApiButton.Name = "testApiButton";
			testApiButton.Size = new System.Drawing.Size(75, 23);
			testApiButton.TabIndex = 19;
			testApiButton.Text = "Test API";
			testApiButton.UseVisualStyleBackColor = true;
			testApiButton.Click += testApiButton_Click;
			// 
			// urlTextInput
			// 
			urlTextInput.Location = new System.Drawing.Point(12, 27);
			urlTextInput.Name = "urlTextInput";
			urlTextInput.Size = new System.Drawing.Size(237, 23);
			urlTextInput.TabIndex = 20;
			urlTextInput.Text = "2";
			// 
			// performanceBox
			// 
			performanceBox.Controls.Add(keyboardCheckBox);
			performanceBox.Controls.Add(mouseCheckBox);
			performanceBox.Controls.Add(label5);
			performanceBox.Controls.Add(frameRateUpDown);
			performanceBox.Location = new System.Drawing.Point(12, 191);
			performanceBox.Name = "performanceBox";
			performanceBox.Size = new System.Drawing.Size(237, 100);
			performanceBox.TabIndex = 22;
			performanceBox.TabStop = false;
			performanceBox.Text = "Performance";
			// 
			// debugBox
			// 
			debugBox.Controls.Add(checkBoxFullscreen);
			debugBox.Controls.Add(testApiButton);
			debugBox.Controls.Add(editButton);
			debugBox.Controls.Add(label1);
			debugBox.Controls.Add(fontComboBox);
			debugBox.Location = new System.Drawing.Point(11, 85);
			debugBox.Name = "debugBox";
			debugBox.Size = new System.Drawing.Size(237, 100);
			debugBox.TabIndex = 23;
			debugBox.TabStop = false;
			debugBox.Text = "Debug";
			// 
			// checkBoxFullscreen
			// 
			checkBoxFullscreen.AutoSize = true;
			checkBoxFullscreen.Location = new System.Drawing.Point(136, 70);
			checkBoxFullscreen.Name = "checkBoxFullscreen";
			checkBoxFullscreen.Size = new System.Drawing.Size(79, 19);
			checkBoxFullscreen.TabIndex = 20;
			checkBoxFullscreen.Text = "Fullscreen";
			checkBoxFullscreen.UseVisualStyleBackColor = true;
			checkBoxFullscreen.CheckedChanged += checkBoxFullscreen_CheckedChanged;
			// 
			// mouseCheckBox
			// 
			mouseCheckBox.AutoSize = true;
			mouseCheckBox.Location = new System.Drawing.Point(152, 15);
			mouseCheckBox.Name = "mouseCheckBox";
			mouseCheckBox.Size = new System.Drawing.Size(62, 19);
			mouseCheckBox.TabIndex = 21;
			mouseCheckBox.Text = "Mouse";
			mouseCheckBox.UseVisualStyleBackColor = true;
			// 
			// keyboardCheckBox
			// 
			keyboardCheckBox.AutoSize = true;
			keyboardCheckBox.Location = new System.Drawing.Point(152, 37);
			keyboardCheckBox.Name = "keyboardCheckBox";
			keyboardCheckBox.Size = new System.Drawing.Size(76, 19);
			keyboardCheckBox.TabIndex = 22;
			keyboardCheckBox.Text = "Keyboard";
			keyboardCheckBox.UseVisualStyleBackColor = true;
			// 
			// SettingsForm
			// 
			AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
			AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			ClientSize = new System.Drawing.Size(261, 311);
			Controls.Add(debugBox);
			Controls.Add(performanceBox);
			Controls.Add(urlTextInput);
			Controls.Add(label2);
			Controls.Add(previewButton);
			Controls.Add(cancelButton);
			Controls.Add(okButton);
			FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			Name = "SettingsForm";
			Load += SettingsForm_Load;
			((System.ComponentModel.ISupportInitialize)frameRateUpDown).EndInit();
			performanceBox.ResumeLayout(false);
			performanceBox.PerformLayout();
			debugBox.ResumeLayout(false);
			debugBox.PerformLayout();
			ResumeLayout(false);
			PerformLayout();
		}

		#endregion

		private System.Windows.Forms.Button okButton;
		private System.Windows.Forms.Button cancelButton;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.NumericUpDown frameRateUpDown;
		private System.Windows.Forms.ComboBox fontComboBox;
		private System.Windows.Forms.Button editButton;
		private System.Windows.Forms.Button previewButton;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button testApiButton;
		private System.Windows.Forms.TextBox urlTextInput;
		private System.Windows.Forms.GroupBox performanceBox;
		private System.Windows.Forms.GroupBox debugBox;
		private System.Windows.Forms.CheckBox checkBoxFullscreen;
		private System.Windows.Forms.CheckBox checkBox2;
		private System.Windows.Forms.CheckBox checkBox1;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.CheckBox checkBox3;
		private System.Windows.Forms.CheckBox checkBox4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.NumericUpDown numericUpDown1;
		private System.Windows.Forms.CheckBox keyboardCheckBox;
		private System.Windows.Forms.CheckBox mouseCheckBox;
	}
}