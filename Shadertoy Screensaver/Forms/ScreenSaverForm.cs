﻿using Microsoft.Win32;
using ShadertoyScreensaver;
using SharpGL;
using ShadertoyScreensaver;
using ShadertoyScreensaver.Scenes;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using ShadertoyScreensaver.Config;

namespace ShadertoyScreensaver
{
	public partial class ScreenSaverForm : Form
	{
		[DllImport("user32.dll")]
		static extern IntPtr SetParent(IntPtr hWndChild, IntPtr hWndNewParent);
		[DllImport("user32.dll")]
		static extern int SetWindowLong(IntPtr hWind, int nIndex, IntPtr dwNewLong);
		[DllImport("user32.dll")]
		static extern int GetWindowLong(IntPtr hWind, int nIndex);
		[DllImport("user32.dll")]
		static extern bool GetClientRect(IntPtr hWind, out Rectangle lpRect);

		private Point mousePoint;
		private bool previewMode = false;

		private BaseScene scene;

		private bool openGLInitialized = false;

		private ShadertoyScreensaverConfig config;

		//Constructor for preview mode
		public ScreenSaverForm(IntPtr previewWndHandle)
		{
			InitializeComponent();

			//Set The preview window as the parent of this window
			SetParent(this.Handle, previewWndHandle);

			//Make this a child window so it will close when the parent closes
			//GWL_STYLE = -16, WS_CHILD = 0x40000000
			SetWindowLong(this.Handle, -16, new IntPtr(GetWindowLong(this.Handle, -16) | 0x40000000));

			//Place our window inside the parent
			Rectangle parentRect;
			GetClientRect(previewWndHandle, out parentRect);
			Size = parentRect.Size;
			Location = new Point(0, 0);

			previewMode = true;
			Setup();

		}
		//Constructor for fullscreen mode
		public ScreenSaverForm(Rectangle bounds)
		{
			InitializeComponent();
			this.Bounds = bounds;
			Setup();
		}


		//Initialization logic that's the same for preivew and normal
		private void Setup()
		{

			config = Helpers.LoadConfig();
			openGLControl1.FrameRate = config.FrameRate;

			//EDIT SCENE HERE!!!!!
			//scene = new LocalGlslScene(openGLControl1.OpenGL);
			//scene = new NativeTexturesExampleScene(openGLControl1.OpenGL);
			scene = new ShadertoyApiScene(openGLControl1.OpenGL);
			TryToInitializeScene();

		}

		//Fire OpenGLInitialized on the Scene if gl is ready and the scene exists
		private void TryToInitializeScene()
		{
			if (scene != null)
			{
				if (openGLInitialized)
				{
					scene.OpenGLInitialized();
				}
			}
		}

		//Form Load Event
		private void ScreenSaverForm_Load(object sender, EventArgs e)
		{
			//Cursor.Hide(); Turn off cursor hiding for now
			TopMost = true;
		}




		//OpenGL Event Passthrough
		private void openGLControl1_OpenGLInitialized(object sender, EventArgs e)
		{
			openGLInitialized = true;
			TryToInitializeScene();
		}

		private void openGLControl1_OpenGLDraw(object sender, RenderEventArgs args)
		{
			//  Draw the scene.
			scene.OpenGLDraw();
		}




		//Screensaver Quitting
		private void openGLControl1_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (config.KeyboardInput)
			{

				scene.InputKeyPress(e);
			}
			else
			{
				Program.KillScreenSaver();
			}
		}

		private void ScreenSaverForm_MouseClick(object sender, MouseEventArgs e)
		{
			/*if (config.MouseInput)
			{
				scene.InputMouseClick(e);
			}
			else
			{
				Program.KillScreenSaver();
			}*/
		}

		private void ScreenSaverForm_MouseMove(object sender, MouseEventArgs e)
		{

			if (config.MouseInput)
			{
				scene.InputMouseMove(e);
			}
			else
			{
				if (!mousePoint.IsEmpty)
				{
					if (Math.Abs(mousePoint.X - e.X) > config.MouseQuitSensitivity || Math.Abs(mousePoint.Y - e.Y) > config.MouseQuitSensitivity)
						Program.KillScreenSaver();
				}
				mousePoint = e.Location;
			}
		}

		public void Quit()
		{
			scene.Dispose();
			if (!previewMode)
			{
				//Application.Exit();
				Close();
			}
		}

		private void openGLControl1_MouseUp(object sender, MouseEventArgs e)
		{
			if (config.MouseInput)
			{
				scene.InputMouseUp(e);
			}
		}

		private void openGLControl1_MouseDown(object sender, MouseEventArgs e)
		{
			if (config.MouseInput)
			{
				scene.InputMouseDown(e);
			}
			else
			{
				Program.KillScreenSaver();
			}
		}
	}
}
