﻿const float PHI = sqrt(5.0)*0.5 + 0.5;

const vec3 gdfVecs[19] = vec3[](
    normalize(vec3(1,0,0)),
    normalize(vec3(0,1,0)),
    normalize(vec3(0,0,1)),
    
    normalize(vec3(1, 1, 1)),
    normalize(vec3(-1, 1, 1)),
    normalize(vec3(1, -1 , 1)),
    normalize(vec3(1, 1, -1)),
    
    normalize(vec3(0, 1, PHI+1.0)),
    normalize(vec3(0, -1, PHI + 1.0)),
    normalize(vec3(PHI + 1.0, 0, 1)),
    normalize(vec3(-PHI-1.0,0, 1)),
    normalize(vec3(1, PHI+1.0, 0)),
    normalize(vec3(-1, PHI+1.0, 0)),
    
    normalize(vec3(0, PHI, 1)),
    normalize(vec3(0, -PHI, 1)),
    normalize(vec3(1, 0, PHI)),
    normalize(vec3(-1, 0, PHI)),
    normalize(vec3(PHI, 1, 0)),
    normalize(vec3(-PHI, 1, 0))
    );


float generalizedDF(vec3 point, float radius, int start, int end)
{
    float retval = 0.0;
    for(int i = start; i <= end; i++)
    {
        retval = max(retval, abs(dot(point, gdfVecs[i])));
    }
    return retval - radius;
}

float generalizedDFExp(vec3 point, float radius, float exponent, int start, int end)
{ 
    float retval = 0.0;
    for(int i = start; i <= end; i++)
    {
        retval += pow(abs(dot(point, gdfVecs[i])), exponent);
    }
    return pow(retval, 1.0/exponent) - radius;
    
}

//THIS IS WHERE YOU MESS AROUND WITH SHAPES
float sceneSDF(vec3 point)
{
    float a = generalizedDFExp(point, 1.0, 60.0, 0, 2);
    float b = generalizedDFExp(point.zyx, 1.0, 60.0, 3, 6);
    float combine = min(a, b);
    
    float pipe = length(vec2(a, b )) - 0.02;
    return b;
    return min(a, min(b, pipe));
    //return generalizedDFExp(point, 1.0, 15.0, 0,2);
    //return length(point)-1.0;
}

vec3 diffuseColor(vec3 point)
{
    float dist = generalizedDFExp(point, 1.5, 60.0, 7, 18);
    if(mod(dist * 5.0, 1.0) <0.5)
    {
        return vec3(0.2, 0.2, 0.2);
    }
    else
    {
        return vec3(0.7, 0.7, 0.7);
    }
}

//Raymarching stuff
const int MAX_MARCHING_STEPS = 255;
const float MIN_DIST = 0.0;
const float MAX_DIST = 100.0;
const float EPSILON = 0.0001;
const float STEP_SCALE =1.0;

float shortestDistanceToSurface(vec3 eye, vec3 direction, float start, float end)
{
  float depth = start;
    for(int i = 0; i < MAX_MARCHING_STEPS; i++)
    {
        float dist = sceneSDF(eye + depth * direction);
        if(dist < EPSILON)
        {
            return depth;
        }
        depth += dist * STEP_SCALE;
        if (depth >= end)
        {
            return end;
        }
    }
    return end;
}

vec3 rayDirection(float fieldOfView, vec2 size, vec2 fragCoord){
    vec2 xy = fragCoord - size/2.0;
    float z = size.y/tan(radians(fieldOfView)/2.0);
    return normalize(vec3(xy,-z));
}

mat4 viewMatrix(vec3 eye, vec3 center, vec3 up)
{
    vec3 f = normalize(center - eye);
    vec3 s = normalize(cross(f, up));
    vec3 u = cross(s, f);
    
    return mat4(
        vec4(s, 0.0),
        vec4(u, 0.0),
        vec4(-f, 0.0),
        vec4(0.0, 0.0, 0.0, 1.0)
        );
        
        
        
}

vec3 estimateNormal(vec3 p)
{
    return normalize(vec3(
        sceneSDF(vec3(p.x + EPSILON, p.y, p.z)) - sceneSDF(vec3(p.x - EPSILON, p.y, p.z)),
        sceneSDF(vec3(p.x, p.y + EPSILON, p.z)) - sceneSDF(vec3(p.x, p.y - EPSILON, p.z)),
        sceneSDF(vec3(p.x, p.y, p.z + EPSILON)) - sceneSDF(vec3(p.x, p.y, p.z-EPSILON))
        ));
}

vec3 phongLight(vec3 diffuse, vec3 specular, float shiny, vec3 point,
                vec3 eye, vec3 lightPos, vec3 lightCol)
{
    vec3 surfaceNormal = estimateNormal(point);
    vec3 lightDir = normalize(lightPos - point);
    vec3 viewDir = normalize(eye - point);
    vec3 R = normalize(reflect(-lightDir, surfaceNormal));
    
    float dotLN = dot(lightDir, surfaceNormal);
    float dotRV = dot(R, viewDir);
    
    if(dotLN < 0.0)
    {
        //light on the other side of the surface
        return vec3(0.0, 0.0, 0.0);
    }
    
    if(dotRV < 0.0)
    {
     //light reflecting away from the camera, no highlight
        return lightCol * (diffuse * dotLN);
    }
    
    return lightCol * (diffuse * dotLN + specular * pow(dotRV, shiny));
}

vec3 phongIllumination(vec3 ambient, vec3 diffuse, vec3 specular, float shiny, vec3 point, vec3 eye)
{
    const vec3 ambientLight = 0.5 * vec3(1.0, 1.0, 1.0);
    vec3 color = ambientLight * ambient;
    
    //vec3 light1Pos = vec3(7.0 * sin(iTime), 2.0, 7.0 * cos(iTime));
    vec3 light1Pos = vec3(7.0 * sin(7.), 2.0, 7.0 * cos(7.));
    vec3 light1Col = vec3(0.8, 0.0, 0.7);
    
    color += phongLight(diffuse, specular, shiny, point, eye, light1Pos, light1Col);
    
    //vec3 light2Pos = vec3(7.0 * sin(iTime), -2.0 * sin(iTime * 0.1), 7.0 * cos(-0.5 * iTime));
    vec3 light2Pos = vec3(7.0 * sin(7.), -2.0 * sin(7. * 0.1), 7.0 * cos(-0.5 * 7.));
    vec3 light2Col = vec3(0.0, 0.6, 0.9);
    
    color += phongLight(diffuse, specular, shiny, point, eye, light2Pos, light2Col);
        
    //vec3 light3Pos = vec3(7.0 * cos(iTime * -.1), 4.0 * cos(iTime * 0.1), 7.0 * sin(-0.5 * iTime));
    vec3 light3Pos = vec3(7.0 * cos(7. * -.1), 4.0 * cos(7. * 0.1), 7.0 * sin(-0.5 * 7.));
    vec3 light3Col = vec3(0.9, 0.5, 0.0);
    
    color += phongLight(diffuse, specular, shiny, point, eye, light3Pos, light3Col);
    
    return color;
    
}

void mainImage( out vec4 fragColor, in vec2 fragCoord )
{
    fragColor = vec4(fragCoord, 0, 1);
    //fragColor = vec4(fragCoord.xy/iResolution.xy, 0, 1);
    //return;
	/*vec2 uv = fragCoord.xy / iResolution.yy;
    vec3 point = vec3(uv, 0.5+0.5*sin(iTime));
    
    point *= vec3(5, 5, 10);
    point -= vec3(4.5, 2.5, 5.0);
    
    
    float dist = sceneSDF(point);
    //float dist = generalizedDFExp(point, 1.0, 99.0, 0, 6);
    
    //vec3 color = dist < 0.0 ?
    //    vec3(1,1,1) : point;
  
    vec3 color = vec3(dist);
	fragColor = vec4(color,1.0);*/
    
    vec3 viewDir = rayDirection(60.0, iResolution.xy, fragCoord);
    
    //fragColor = vec4(viewDir, 0) ;
    //return;

    vec3 eye = vec3(4.0 * sin(iTime * 0.5), 1.5 * sin(iTime), 4.0 * cos(iTime * 0.5));
    //vec3 eye = vec3(4.0 * sin(7. * 0.5), 1.5 * sin(7.), 4.0 * cos(7. * 0.5));
    
    mat4 viewToWorld = viewMatrix(eye, vec3(0.0, 0.0, 0.0), vec3(0.0, 1.0, 0.0));
    
    vec3 worldDir = (viewToWorld * vec4(viewDir, 0.0)).xyz;
  
    
    float dist = shortestDistanceToSurface(eye, worldDir, MIN_DIST, MAX_DIST);
    
      
    //fragColor = vec4(dist, dist * 0.1, dist * 0.01,1);
    //return;
    
    if(dist > MAX_DIST - EPSILON)
    {
        fragColor = vec4(0.0, 0.0, 0.0, 1.0);
    }
    else
    {
        
        vec3 point = eye + dist * worldDir;
        
        //THIS IS WHERE YOU MESS AROUND WITH COLOR
        vec3 diffuse = diffuseColor(point);
        
        vec3 ambient = vec3(0.2, 0.2, 0.2);
        vec3 specular = diffuse  -vec3(0.1, 0.1, 0.1);
        float shiny = 10.0 * specular.x;
        
        vec3 litColor = phongIllumination(ambient, diffuse, specular, shiny, point, eye);
        
        fragColor = vec4(litColor, 1.0);
    }
}

