﻿#version 150 core

in vec3 in_Position;
in vec3 in_Color;  
out vec3 pass_Color;
out vec2 fragCoord;

uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;
uniform mat4 modelMatrix;



uniform vec3      iResolution;           // viewport resolution (in pixels)

void main(void) {
	gl_Position = projectionMatrix * viewMatrix * modelMatrix * vec4(in_Position, 1.0);
	pass_Color = in_Color;
	fragCoord = in_Color.xy * iResolution.xy;
}