﻿using Newtonsoft.Json;
using ShadertoyScreensaver.Config;
using System;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace ShadertoyScreensaver
{
	public static class Helpers
	{
		public static string LoadTextFile(string textFileName)
		{
			string path = Path.GetDirectoryName(Process.GetCurrentProcess().MainModule.FileName) + textFileName;
			if (File.Exists(path))
			{
				using (var reader = new StreamReader(path))
				{
					return reader.ReadToEnd();
				}
			}
			else
			{
				MessageBox.Show("Could not load file: " + path);
				return null;
			}
		}

		public static string LocalPathFromShadertoySrc(string src)
		{
			string fileName = Path.GetFileName(src);
			string localFile = Path.Join(PathStrings.ASSETS_FOLDER, fileName);
			return localFile;
		}

		public static bool BoolFromString(string value)
		{
			switch (value.ToLower())
			{
				case "true":
					return true;
				case "false":
					return false;
				default:
					throw new Exception($"{value} is not true or false.");
			}
		}

		public static ShadertoyScreensaverConfig LoadConfig()
		{
			if (File.Exists(ShadertoyScreensaverConfig.SavePath))
			{
				var json = File.ReadAllText(ShadertoyScreensaverConfig.SavePath);
				return JsonConvert.DeserializeObject<ShadertoyScreensaverConfig>(json);
			}
				return new ShadertoyScreensaverConfig();
			
		}
	}
}
