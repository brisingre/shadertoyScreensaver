﻿using GlmNet;
using ShadertoyScreensaver;
using SharpGL;
using SharpGL.Shaders;
using SharpGL.VertexBuffers;
using ShadertoyScreensaver.Scenes;
using System;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using static ShadertoyScreensaver.Scenes.ShadertoyApiScene;
using ShadertoyScreensaver.API;
using ShadertoyScreensaver.Textures;
using System.Collections;
using System.IO;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Processing.Processors.Quantization;

namespace ShadertoyScreensaver
{
	internal class RenderPassBuffer : IDisposable
	{
		ShaderToyDataRenderPass data;
		Framebuffer framebuffer;
		ShaderProgram shaderProgram;

		Texture[] textures = new Texture[4];


		VertexBufferArray vertexBufferArray;

		public RenderPassBuffer(OpenGL gl, ShaderToyDataRenderPass data, int width, int height)
		{
			Debug.WriteLine($"Creating RenderPassBuffer:{data.Name}");
			this.data = data;

			framebuffer = new Framebuffer(gl, width, height);
			
		}

		public void Setup(OpenGL gl, ShadertoyApiScene scene)
		{
			vertexBufferArray = scene.shaderGlobals.vertexBufferArray;

			foreach (var input in data.Inputs)
			{
				SetupTextureFromInput(gl, scene, input);
			}

			shaderProgram = GlslFactory.CreateShaderProgramFromShadertoyRenderPass(gl, data, textures, scene.commonRenderPassCode);
			shaderProgram.Bind(gl);

			//Set MVP Matrices
			shaderProgram.SetUniformMatrix4(gl, "projectionMatrix", scene.shaderGlobals.projectionMatrix.to_array());
			shaderProgram.SetUniformMatrix4(gl, "viewMatrix", scene.shaderGlobals.viewMatrix.to_array());
			shaderProgram.SetUniformMatrix4(gl, "modelMatrix", scene.shaderGlobals.modelMatrix.to_array());

			// Bind the textures
			gl.Uniform1(shaderProgram.GetUniformLocation(gl, "iChannel0"), 0);
			gl.Uniform1(shaderProgram.GetUniformLocation(gl, "iChannel1"), 1);
			gl.Uniform1(shaderProgram.GetUniformLocation(gl, "iChannel2"), 2);
			gl.Uniform1(shaderProgram.GetUniformLocation(gl, "iChannel3"), 3);


		}

		void SetupTextureFromInput(OpenGL gl, ShadertoyApiScene scene, ShaderToyDataInput input)
		{
			Debug.WriteLine($"Creating texture from {input.Src} in slot {input.Channel}");
			var bytes = scene.shaderToyAssets[input.Src];

			Texture texture = null;
			uint textureId = 0;



			switch (input.Ctype)
			{
				case "buffer":
					int bufferId = input.Id - 257; //Buffer ids are 257-260
					if(scene.renderPassBuffers[bufferId] != null)
					{
						texture = scene.renderPassBuffers[bufferId].framebuffer.texture;
						scene.renderPassBuffers[bufferId].framebuffer.ApplyTextureSettings(OpenGlEnums.FilterModeFromString(input.Sampler.Filter), OpenGlEnums.WrapModeFromString(input.Sampler.Wrap));
					}
					else
					{
						Debug.WriteLine("Looks like this shader references a buffer it doesn't actually use.");
						texture = new Texture2D(gl);
					}
					break;
				case "cubemap":
					var cubemap = Image.Load<Rgba32>(bytes);
					texture = new Cubemap(gl, cubemap, input.Sampler);
					break;
				case "texture":
					var bitmap = Image.Load(bytes);
					texture = new Texture2D(gl, bitmap, input.Sampler);
					break;
				case "volume":
					texture = new Texture3D(gl, bytes, input.Sampler);
					break;
				case "video":
					texture = new Video(gl, Helpers.LocalPathFromShadertoySrc(input.Src), input.Sampler);
					break;
				default:
					throw new Exception($"Can't handle texture Ctype {input.Ctype}");
			}

			if (texture == null)
				throw new Exception("Texture is null!");

			textures[input.Channel] = texture;
		}

		public void DrawShaderToBuffer(OpenGL gl, ShaderGlobals shaderGlobals)
		{
			framebuffer.Bind();
			DrawShader(gl, shaderGlobals); 
			framebuffer.Unbind();
		}

		public void DrawShaderToScreen(OpenGL gl, ShaderGlobals shaderGlobals)
		{
			DrawShader(gl, shaderGlobals);
		}

		void DrawShader(OpenGL gl, ShaderGlobals shaderGlobals)
		{
			if (shaderProgram == null)
				return;

			foreach(var texture in textures)
			{
				texture?.Update();
			}

			shaderProgram.Bind(gl);
			//Set Shadertoy uniforms from globals
			shaderProgram.SetUniform1(gl, "iTime", shaderGlobals.iTime);
			shaderProgram.SetUniform1(gl, "iTimeDelta", shaderGlobals.iTimeDelta);
			shaderProgram.SetUniform1(gl, "iFrameRate", shaderGlobals.iFrameRate);
			gl.Uniform1(shaderProgram.GetUniformLocation(gl, "iFrame"), shaderGlobals.iFrame);
			gl.Uniform4(shaderProgram.GetUniformLocation(gl, "iDate"), 1, shaderGlobals.iDate);
			gl.Uniform4(shaderProgram.GetUniformLocation(gl, "iMouse"), 1, shaderGlobals.iMouse);


			//set Shadertoy uniforms from internal stuff
			shaderProgram.SetUniform3(gl, "iResolution", framebuffer.width, framebuffer.height, 0f);

			float[] iChannelTime = new float[] {
				textures[0] != null ? textures[0].GetChannelTime() : 0,
				textures[1]!= null? textures[1].GetChannelTime() : 0,
				textures[2]!= null? textures[2].GetChannelTime() : 0,
				textures[3]!= null? textures[3].GetChannelTime() : 0};

			gl.Uniform1(shaderProgram.GetUniformLocation(gl, "iChannelTime"), 4, iChannelTime);

			float[] iChannelResolution = new float[0];
			foreach (var tex in textures)
				if(tex == null)
					iChannelResolution.Concat(new float[]{ 0, 0, 0, 0});
				else
					iChannelResolution = iChannelResolution.Concat(tex.GetChannelResolution()).ToArray();

			gl.Uniform3(shaderProgram.GetUniformLocation(gl, "iChannelResolution"), 4, iChannelResolution);


			for (uint i = 0; i < 4; i++)
			{
				gl.ActiveTexture(OpenGL.GL_TEXTURE0 + i);
				textures[i]?.BindTexture();
			}

			vertexBufferArray.Bind(gl);
			gl.DrawArrays(OpenGL.GL_TRIANGLES, 0, 6);
			vertexBufferArray.Unbind(gl);

			shaderProgram.Unbind(gl);
		}

		public void Dispose()
		{
			framebuffer.Dispose();
			foreach(var texture in textures)
					texture?.Dispose();

		}
	}
}
