﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;
using GlmNet;
using SharpGL;
using SharpGL.Enumerations;
using SharpGL.Shaders;
using SharpGL.VertexBuffers;
using ShadertoyScreensaver.Textures;
using ShadertoyScreensaver.API;


namespace ShadertoyScreensaver
{
	internal static class GlslFactory
	{


		//  Constants that specify the attribute indexes.
		const uint attributeIndexPosition = 0;
		const uint attributeIndexColour = 1;


		/// <summary>
		/// Creates the geometry for the square, also creating the vertex buffer array.
		/// </summary>
		/// <param name="gl">The OpenGL instance.</param>
		public static VertexBufferArray CreateVerticesForSquare(OpenGL gl)
		{
			var vertices = new float[18];
			var colors = new float[18]; // Colors for our vertices  
			vertices[0] = 0f; vertices[1] = 0f; vertices[2] = 0.0f; // Bottom left corner  
			colors[0] = 0.0f; colors[1] = 0.0f; colors[2] = 0.0f; // Bottom left corner  
			vertices[3] = 0f; vertices[4] = 1f; vertices[5] = 0.0f; // Top left corner  
			colors[3] = 0.0f; colors[4] = 1.0f; colors[5] = 0.0f; // Top left corner  
			vertices[6] = 1f; vertices[7] = 1f; vertices[8] = 0.0f; // Top Right corner  
			colors[6] = 1.0f; colors[7] = 1.0f; colors[8] = 0.0f; // Top Right corner  
			vertices[9] = 1f; vertices[10] = 0f; vertices[11] = 0.0f; // Bottom right corner  
			colors[9] = 1.0f; colors[10] = 0.0f; colors[11] = 0.0f; // Bottom right corner  
			vertices[12] = 0f; vertices[13] = 0f; vertices[14] = 0.0f; // Bottom left corner  
			colors[12] = 0.0f; colors[13] = 0.0f; colors[14] = 0.0f; // Bottom left corner  
			vertices[15] = 1f; vertices[16] = 1f; vertices[17] = 0.0f; // Top Right corner  
			colors[15] = 1.0f; colors[16] = 1.0f; colors[17] = 0.0f; // Top Right corner  

			//  Create the vertex array object.
			var vertexBufferArray = new VertexBufferArray();
			vertexBufferArray.Create(gl);
			vertexBufferArray.Bind(gl);

			//  Create a vertex buffer for the vertex data.
			var vertexDataBuffer = new VertexBuffer();
			vertexDataBuffer.Create(gl);
			vertexDataBuffer.Bind(gl);
			vertexDataBuffer.SetData(gl, 0, vertices, false, 3);

			//  Now do the same for the colour data.
			var colourDataBuffer = new VertexBuffer();
			colourDataBuffer.Create(gl);
			colourDataBuffer.Bind(gl);
			colourDataBuffer.SetData(gl, 1, colors, false, 3);



			//  Unbind the vertex array, we've finished specifying data for it.
			vertexBufferArray.Unbind(gl);
			return vertexBufferArray;
		}


		public static ShaderProgram CreateShaderProgramFromLocalGlslFile(OpenGL gl, string shaderName)
		{

			//  Create the shader program.
			var vertexShaderSource = Helpers.LoadTextFile("\\Shaders\\MainVert.vert");
			var fragmentShaderSource = Helpers.LoadTextFile("\\Shaders\\MainFrag.frag");
			var shadertoySource = Helpers.LoadTextFile("\\Shaders\\Shadertoy\\" + shaderName + ".glsl");

			fragmentShaderSource = fragmentShaderSource.Replace("//INCLUDE_SHADERTOY_RENDERPASS", shadertoySource);
			var shaderProgram = new ShaderProgram();

			shaderProgram.Create(gl, vertexShaderSource, fragmentShaderSource, null);
			shaderProgram.BindAttributeLocation(gl, attributeIndexPosition, "in_Position");
			shaderProgram.BindAttributeLocation(gl, attributeIndexColour, "in_Color");
			shaderProgram.AssertValid(gl);

			return shaderProgram;

		}

		public static ShaderProgram CreateShaderProgramFromShadertoyRenderPass(OpenGL gl, ShaderToyDataRenderPass stdRenderPass, Texture[] textures, string commonCode = "")
		{
			try
			{
				// Load vertex and fragment shader source code
				var vertexShaderSource = Helpers.LoadTextFile("\\Shaders\\MainVert.vert");
				if (string.IsNullOrEmpty(vertexShaderSource))
				{
					MessageBox.Show("Vertex shader source could not be loaded.");
					return null;
				}

				var fragmentShaderSource = Helpers.LoadTextFile("\\Shaders\\MainFrag.frag");
				if (string.IsNullOrEmpty(fragmentShaderSource))
				{
					MessageBox.Show("Fragment shader source could not be loaded.");
					return null;
				}


				// Insert the ShaderToy code into the fragment shader template
				var shadertoySource = stdRenderPass.Code;
				fragmentShaderSource = fragmentShaderSource.Replace(GlslSnippets.INCLUDE_SHADERTOY_COMMON, commonCode);
				fragmentShaderSource = fragmentShaderSource.Replace(GlslSnippets.INCLUDE_SHADERTOY_RENDERPASS, shadertoySource);


				string defaultSamplerString = GlslSnippets.UNIFORM_SAMPLER_2D;

				fragmentShaderSource = fragmentShaderSource.Replace(GlslSnippets.INCLUDE_SHADERTOY_SAMPLER_0, textures[0] != null? textures[0].GetShaderUniform() : defaultSamplerString);
				fragmentShaderSource = fragmentShaderSource.Replace(GlslSnippets.INCLUDE_SHADERTOY_SAMPLER_1, textures[1] != null ? textures[1].GetShaderUniform() : defaultSamplerString);
				fragmentShaderSource = fragmentShaderSource.Replace(GlslSnippets.INCLUDE_SHADERTOY_SAMPLER_2, textures[2] != null ? textures[2].GetShaderUniform() : defaultSamplerString);
				fragmentShaderSource = fragmentShaderSource.Replace(GlslSnippets.INCLUDE_SHADERTOY_SAMPLER_3, textures[3] != null ? textures[3].GetShaderUniform() : defaultSamplerString);



				//Debug.WriteLine(fragmentShaderSource);

				// Create the shader program
				var attributeLocations = new Dictionary<uint, string>
				{
					//{ attributeIndexPosition, "in_Position" },
					//{ attributeIndexColour, "in_Color" }
				};
				var shaderProgram = new ShaderProgram();
				shaderProgram.Create(gl, vertexShaderSource, fragmentShaderSource, attributeLocations);

				// Check for OpenGL errors after creating the shader program
				CheckGLError(gl, "Create Shader Program");

				// Bind attribute locations
				//shaderProgram.BindAttributeLocation(gl, attributeIndexPosition, "in_Position");
				//shaderProgram.BindAttributeLocation(gl, attributeIndexColour, "in_Color");

				// Validate the shader program
				shaderProgram.AssertValid(gl);

				// Check for OpenGL errors after validating the shader program
				CheckGLError(gl, "Validate Shader Program");

				return shaderProgram;
			}
			catch (Exception ex)
			{
				MessageBox.Show("An error occurred while creating the shader program: " + ex.Message);

				return null;
			}
		}

		public static ShaderProgram CreateShaderProgramFromShadertoyRenderPassOld(OpenGL gl, ShaderToyDataRenderPass stdRenderPass)
		{

			//  Create the shader program.
			var vertexShaderSource = Helpers.LoadTextFile("\\Shaders\\MainVert.vert");
			var fragmentShaderSource = Helpers.LoadTextFile("\\Shaders\\MainFrag.frag");

			var shadertoySource = stdRenderPass.Code;
			fragmentShaderSource = fragmentShaderSource.Replace("//INCLUDE_SHADERTOY", shadertoySource);
			var shaderProgram = new ShaderProgram();

			shaderProgram.Create(gl, vertexShaderSource, fragmentShaderSource, null);
			shaderProgram.BindAttributeLocation(gl, attributeIndexPosition, "in_Position");
			shaderProgram.BindAttributeLocation(gl, attributeIndexColour, "in_Color");
			shaderProgram.AssertValid(gl);
			return shaderProgram;


		}

		private static bool CheckGLError(OpenGL gl, string operation)
		{
			var errorCode = gl.GetError();
			if (errorCode != (uint)ErrorCode.NoError)
			{
				//MessageBox.Show($"OpenGL Error({errorCode}) during: {operation}");
				Debug.WriteLine($"OpenGL Error({errorCode}) [{(ErrorCode)errorCode}] during: {operation}");
				return true; // Error occurred
			}
			return false; // No error
		}
	}
}
